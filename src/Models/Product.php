<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\ProductProxy;
use PlugDeploy\PlugCommerce\Contracts\Product as ProductContract;

class Product extends Model implements ProductContract
{
    use HasFactory;

    protected $table = 'products';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\ProductFactory::new();
    }
}
