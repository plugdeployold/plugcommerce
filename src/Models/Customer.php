<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\CustomerProxy;
use PlugDeploy\PlugCommerce\Contracts\Customer as CustomerContract;

class Customer extends Model implements CustomerContract
{
    use HasFactory;

    protected $table = 'customers';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\CustomerFactory::new();
    }
}
