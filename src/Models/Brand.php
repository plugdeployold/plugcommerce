<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\BrandProxy;
use PlugDeploy\PlugCommerce\Contracts\Brand as BrandContract;

class Brand extends Model implements BrandContract
{
    use HasFactory;

    protected $table = 'brands';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\BrandFactory::new();
    }
}
