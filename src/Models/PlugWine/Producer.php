<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Producer as ProducerContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\ProducerFactory;

class Producer extends Model implements ProducerContract
{
    use HasFactory;

    protected $table = 'producers';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\ProducerFactory::new();
    }
}
