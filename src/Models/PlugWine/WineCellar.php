<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineCellar as WineCellarContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineCellarFactory;

class WineCellar extends Model implements WineCellarContract
{
    use HasFactory;

    protected $table = 'wine_cellars';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineCellarFactory::new();
    }
}
