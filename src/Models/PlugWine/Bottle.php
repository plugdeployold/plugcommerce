<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Bottle as BottleContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\BottleFactory;

class Bottle extends Model implements BottleContract
{
    use HasFactory;

    protected $table = 'bottles';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\BottleFactory::new();
    }
}
