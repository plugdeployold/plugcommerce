<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineTastingNote as WineTastingNoteContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineTastingNoteFactory;

class WineTastingNote extends Model implements WineTastingNoteContract
{
    use HasFactory;

    protected $table = 'wine_tasting_notes';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineTastingNoteFactory::new();
    }
}
