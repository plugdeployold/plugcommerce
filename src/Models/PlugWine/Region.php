<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Region as RegionContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\RegionFactory;

class Region extends Model implements RegionContract
{
    use HasFactory;

    protected $table = 'regions';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\RegionFactory::new();
    }
}
