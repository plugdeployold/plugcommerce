<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Capacity as CapacityContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\CapacityFactory;

class Capacity extends Model implements CapacityContract
{
    use HasFactory;

    protected $table = 'capacities';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\CapacityFactory::new();
    }
}
