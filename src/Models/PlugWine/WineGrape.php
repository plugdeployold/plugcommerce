<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineGrape as WineGrapeContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineGrapeFactory;

class WineGrape extends Model implements WineGrapeContract
{
    use HasFactory;

    protected $table = 'wine_grapes';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineGrapeFactory::new();
    }
}
