<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineState as WineStateContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineStateFactory;

class WineState extends Model implements WineStateContract
{
    use HasFactory;

    protected $table = 'wine_states';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineStateFactory::new();
    }
}
