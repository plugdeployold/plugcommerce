<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\BottleCapacity as BottleCapacityContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\BottleCapacityFactory;

class BottleCapacity extends Model implements BottleCapacityContract
{
    use HasFactory;

    protected $table = 'bottle_capacities';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\BottleCapacityFactory::new();
    }
}
