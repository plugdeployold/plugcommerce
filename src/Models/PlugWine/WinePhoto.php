<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WinePhoto as WinePhotoContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WinePhotoFactory;

class WinePhoto extends Model implements WinePhotoContract
{
    use HasFactory;

    protected $table = 'wine_photos';

    protected $fillable = [
        'name',
        'url'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WinePhotoFactory::new();
    }
}
