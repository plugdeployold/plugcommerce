<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineType as WineTypeContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineTypeFactory;

class WineType extends Model implements WineTypeContract
{
    use HasFactory;

    protected $table = 'wine_types';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineTypeFactory::new();
    }
}
