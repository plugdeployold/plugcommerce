<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineCellarSection as WineCellarSectionContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineCellarSectionFactory;

class WineCellarSection extends Model implements WineCellarSectionContract
{
    use HasFactory;

    protected $table = 'wine_cellar_sections';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineCellarSectionFactory::new();
    }
}
