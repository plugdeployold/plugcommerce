<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Wine as WineContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineFactory;

class Wine extends Model implements WineContract
{
    use HasFactory;

    protected $table = 'wines';

    protected $fillable = [
        'name',
        'country_id',
        'producer_id',
        'quinta_id',
        'region_id',
        'wine_type_id',
        'wine_variety_id',
        'winemaker_id',
        'description',
        'temp_min',
        'temp_max',
        'alcohol_percentage',
        'harvest_year',
        'bottling_year',
        'time_wine_stage',
        'status'
    ];

    protected static function newFactory()
    {
        //return \WineCellar\StockSystem\Database\Factories\WineFactory::new();
    }
}
