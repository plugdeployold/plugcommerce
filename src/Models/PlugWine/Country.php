<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Country as CountryContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\CountryFactory;

class Country extends Model implements CountryContract
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\CountryFactory::new();
    }
}
