<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineMaker as WineMakerContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineMakerFactory;

class WineMaker extends Model implements WineMakerContract
{
    use HasFactory;

    protected $table = 'wine_makers';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineMakerFactory::new();
    }
}
