<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineFarm as WineFarmContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineFarmFactory;

class WineFarm extends Model implements WineFarmContract
{
    use HasFactory;

    protected $table = 'wine_farms';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineFarmFactory::new();
    }
}
