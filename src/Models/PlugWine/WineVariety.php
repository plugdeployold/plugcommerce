<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\WineVariety as WineVarietyContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\WineVarietyFactory;

class WineVariety extends Model implements WineVarietyContract
{
    use HasFactory;

    protected $table = 'wine_varieties';

    protected $fillable = [
        'name',
        'status'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\WineVarietyFactory::new();
    }
}
