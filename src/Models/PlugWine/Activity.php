<?php

namespace PlugDeploy\PlugCommerce\Models\PlugWine;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use PlugDeploy\PlugCommerce\Contracts\PlugWine\Activity as ActivityContract;
use PlugDeploy\PlugCommerce\Factories\PlugWine\ActivityFactory;

class Activity extends Model implements ActivityContract
{
    use HasFactory;

    protected $table = 'activities';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \WineCellar\StockSystem\Database\Factories\ActivityFactory::new();
    }
}
