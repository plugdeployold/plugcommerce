<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\AttributeProxy;
use PlugDeploy\PlugCommerce\Contracts\Attribute as AttributeContract;

class Attribute extends Model implements AttributeContract
{
    use HasFactory;

    protected $table = 'attributes';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\AttributeFactory::new();
    }
}
