<?php

namespace PlugDeploy\PlugCommerce\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'cms__pages';
    
    public $translatedAttributes = [
        'page_id',
        'title',
        'slug',
        'status',
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type',
    ];

    protected $fillable = [
        'id',
        'name',
        'type',
        'is_home',
        'slug',
        'controller_name',
        'route_name',
        'published',
        'status',
    ];
}

/*
        'body',
        'meta_title',
        'meta_description',
        'og_title',
        'og_description',
        'og_image',
        'og_type', */