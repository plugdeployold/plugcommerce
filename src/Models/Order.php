<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\OrderProxy;
use PlugDeploy\PlugCommerce\Contracts\Order as OrderContract;

class Order extends Model implements OrderContract
{
    use HasFactory;

    protected $table = 'orders';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\OrderFactory::new();
    }
}
