<?php

namespace PlugDeploy\PlugCommerce\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PlugDeploy\PlugCommerce\Models\CategoryProxy;
use PlugDeploy\PlugCommerce\Contracts\Category as CategoryContract;

class Category extends Model implements CategoryContract
{
    use HasFactory;

    protected $table = 'categories';

    protected $fillable = [
        'name'
    ];

    protected static function newFactory()
    {
        return \PlugDeploy\PlugCommerce\Database\Factories\CategoryFactory::new();
    }
}
