<?php

namespace PlugDeploy\PlugCommerce\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\View;

use Illuminate\Filesystem\Filesystem;

class PlugCommerceServiceProvider extends ServiceProvider
{
    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', 'PlugDeploy\PlugCommerce\Http\View\Composers\MenuComposer');

        View::composer('*', 'PlugDeploy\PlugCommerce\Http\View\Composers\DarkModeComposer');

        View::composer('*', 'PlugDeploy\PlugCommerce\Http\View\Composers\FakerComposer');

        View::composer('*', 'PlugDeploy\PlugCommerce\Http\View\Composers\LoggedInUserComposer');

        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/admin-routes.php');

        $this->loadRoutesFrom(__DIR__ . '/../Http/Routes/shop-routes.php');

        $this->loadViewsFrom(__DIR__ . '/../Resources/views/admin', 'plugcommerce');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views/layout', 'layout');


        $this->loadViewsFrom(__DIR__ . '/../Resources/views/shop', 'shop');

          // Load Views - Plug Wine - 08-04-2021 - Jean-Pierre Carvalho
        $this->loadViewsFrom(__DIR__ . '/../Resources/views/plugwine', 'plugwine');

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');

        // Load Migrations - Plug Wine - 08-04-2021 - Jean-Pierre Carvalho
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations/PlugWine');

        // Load Migrations - CMS - 19-04-2021 - Jean-Pierre Carvalho
        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations/CMS');

        $this->loadFactoriesFrom(__DIR__ . '/../Database/Factories');

        $this->loadTranslationsFrom(__DIR__ . '/../Resources/lang', 'plugcommerce');

        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('plugcommerce/admin/default/assets'),
        ], 'public');
    }

    protected function registeredRepositories()
    {
        return [
            '\WineCellar\StockSystem\Repositories\Wine\WineRepositoryInterface' => '\WineCellar\StockSystem\Repositories\Wine\WineRepository',
        ];
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConfig();

        // Register all repositories
        $repos = $this->registeredRepositories();

        foreach ($repos as $interface => $implemented) {
            $this->app->bind($interface, $implemented);
        }
    }

    /**
     * Register package config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/admin-menu.php', 'menu.admin'
        );

        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/acl.php', 'acl'
        );

        $this->mergeConfigFrom(
            dirname(__DIR__) . '/Config/system.php', 'core'
        );
    }
}

/* Starting reading all route files inside the module */

        //$files = new Filesystem();

        //$this->files = $files;

        // Automated Routes
        /*foreach ($this->files->allFiles(__DIR__ . '/../Http/AutomatedRoutes') as $file){
            $this->loadRoutesFrom(__DIR__ . '/../Http/AutomatedRoutes/' . $file->getFilename());
        }*/

        // Routes
        

        /* End */

       


        /*$this->loadRoutesFrom(__DIR__ . '/../Http/shop-routes.php');



        $this->publishes([
            __DIR__ . '/../../publishable/assets' => public_path('themes/default/assets'),
        ], 'public');



        Event::listen('bagisto.admin.layout.head', function ($viewRenderEventManager) {
            $viewRenderEventManager->addTemplate('stocksystem::admin.layouts.style');
        });*/