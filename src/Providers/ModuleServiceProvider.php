<?php

namespace WineCellar\StockSystem\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \WineCellar\StockSystem\Models\Wine::class,
        \WineCellar\StockSystem\Models\Bottle::class,
    ];
}
