@extends('layout::side-menu')

@section('subhead')
    <title>PlugCommerce | {{-- $listingInfo['PageTitle'] --}}</title>
@endsection

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-2 mb-2">
        <h2 class="text-lg font-medium mr-auto">Gestão de Garrafas</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
                <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700 dark:text-gray-300"
                        data-feather="search"></i>
                    <input type="text"
                        class="form-control w-full sm:w-64 box px-10 text-gray-700 dark:text-gray-300 placeholder-theme-13 mr-2"
                        placeholder="Procurar Garrafas">
                    <div class="inbox-filter dropdown absolute inset-y-0 mr-3 right-0 flex items-center"
                        data-placement="bottom-start">
                        <i class="dropdown-toggle w-4 h-4 cursor-pointer text-gray-700 dark:text-gray-300" role="button"
                            aria-expanded="false" data-feather="chevron-down"></i>
                        <div class="inbox-filter__dropdown-menu dropdown-menu pt-2">
                            <div class="dropdown-menu__content box p-5">
                                <div class="grid grid-cols-12 gap-4 gap-y-3">
                                    <div class="col-span-6">
                                        <label for="input-filter-1" class="form-label text-xs">Nome</label>
                                        <input id="input-filter-1" type="text" class="form-control flex-1"
                                            placeholder="Insira um nome">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-2" class="form-label text-xs">SKU</label>
                                        <input id="input-filter-2" type="text" class="form-control flex-1"
                                            placeholder="Insira um sku">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-3" class="form-label text-xs">Categoria</label>
                                        <select id="input-filter-4" class="form-select flex-1">
                                            {{-- <option>10</option>
                                            <option>25</option>
                                            <option>35</option>
                                            <option>50</option> --}}
                                        </select>
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-4" class="form-label text-xs">Marca</label>
                                        <select id="input-filter-4" class="form-select flex-1">
                                            {{-- <option>10</option>
                                            <option>25</option>
                                            <option>35</option>
                                            <option>50</option> --}}
                                        </select>
                                    </div>
                                    <div class="col-span-12 flex items-center mt-3">
                                        <!--<button class="btn btn-secondary w-32 ml-auto">Create Filter</button>-->
                                        <button class="btn btn-primary w-32 ml-auto">Procurar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn btn-primary shadow-md" href="{{ route('plugcommerce.admin.products.create') }}">Adicionar
                Garrafa</a>
        </div>
    </div>
    <div class="intro-y grid grid-cols-12 gap-6 mt-4">
        <!-- BEGIN: Blog Layout -->
        @foreach ($bottles as $bottle)
            <div class="intro-y col-span-12 md:col-span-6 xl:col-span-3 box zoom-in">
                <div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
                    <div class="w-10 h-10 flex-none image-fit">
                        <img alt="alt image" class="rounded-full" src="{{ $bottle->imageSrc }}">
                    </div>
                    <div class="ml-3 mr-auto">
                        <a href="" class="font-medium">{{ $bottle->name }} </a>
                        <div class="flex text-gray-600 truncate text-xs mt-0.5">
                            <a class="text-theme-1 dark:text-theme-10 inline-block truncate"
                                href="">{{ $bottle->category }}
                            </a> <span class="mx-1">•</span>
                            {{ $bottle->name }}
                        </div>
                    </div>
                    <div class="dropdown ml-3">
                        <a href="javascript:;" class="dropdown-toggle w-5 h-5 text-gray-600 dark:text-gray-300"
                            aria-expanded="false">
                            <i data-feather="more-vertical" class="w-4 h-4"></i>
                        </a>
                        <div class="dropdown-menu w-40">
                            <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                <form action="{{ route('plugcommerce.admin.products.edit', $bottle->id) }}" method="GET">
                                    @csrf()
                                    <button type="submit"
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="edit-2" class="w-4 h-4 mr-2"></i>
                                        Editar Garrafa </button>
                                </form>
                                <form action="{{ route('plugcommerce.admin.products.destroy', $bottle->id) }}"
                                    method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @method('DELETE')
                                    @csrf()
                                    <button type="submit"
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i>
                                        Apagar Garrafa </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-5">
                    <div class="h-40 xxl:h-56 image-fit">
                        <img alt="alt image" class="rounded-md" src="{{ $bottle->imageSrc }}">
                    </div>
                    <a href="" class="block font-medium text-base mt-5">{{ $bottle->name }} </a>
                    <div class="text-gray-700 dark:text-gray-600 mt-2">{{ $bottle->description }} </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- BEGIN: Pagination -->
    {{-- flex flex-wrap sm:flex-row sm:flex-nowrap --}}
    <div class="intro-y col-span-12 mt-5 p-5 box">
        <div class="flex">
            <div class="flex-none w-50">
                <a class="btn btn-primary shadow-md mr-2"> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
                <a class="btn btn-primary shadow-md mr-2"> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>

            </div>
            <div class="flex-grow text-center">
                <a class="btn shadow-md ml-5" href="">1</a>
                <a class="btn btn-primary shadow-md ml-5 mr-5" href="">2</a>
                <a class="btn shadow-md mr-5" href="">3</a>
            </div>
            <div class="flex-none w-50">

                <a class="btn btn-primary shadow-md ml-2"> <i class="w-4 h-4" data-feather="chevron-right"></i></a>
                <a class="btn btn-primary shadow-md ml-2"> <i class="w-4 h-4" data-feather="chevrons-right"></i></a>

            </div>
            {{-- <select class="w-20 form-select ">
                    <option>6</option>
                    <option>12</option>
                    <option>18</option>
                    <option>24</option>
                </select> --}}
        </div>

    </div>
    <!-- END: Pagination -->
@endsection
