<html>

<head>
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/_tailwind.css') }}" />

    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <style>
        html {
            background-color: white !important;
        }

    </style>
</head>

<body>
    {{-- Header --}}
    <div class="container mx-auto p-4">
        <div class="mx-auto">
            <div
                class="flex justify-between items-center border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10">
                <div class="flex justify-start lg:w-0 lg:flex-1">
                    <a href="#">
                        <span class="sr-only">PlugDeploy</span>
                        <img class="h-8 w-auto sm:h-10"
                            src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="">
                    </a>
                </div>
                <div>
                    <img width="150px"
                        src="https://www.lorealparis.pt/-/media/project/loreal/brand-sites/oap/shared/baseline/navigationext/loreal-paris-black-logo.svg" />
                </div>
                {{-- <nav class="hidden md:flex space-x-10">

                    <a href="#" class="text-base font-medium text-gray-500 hover:text-gray-900">
                        Pricing
                    </a>
                    <a href="#" class="text-base font-medium text-gray-500 hover:text-gray-900">
                        Docs
                    </a>

                </nav> --}}
                <div class="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
                    {{-- <a href="#" class="whitespace-nowrap text-base font-medium text-gray-500 hover:text-gray-900">
                        Entrar
                    </a>
                    <a href="#"
                        class="ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-indigo-600 hover:bg-indigo-700">
                        Criar Conta
                    </a> --}}

                    <div class="intro-x dropdown">
                        <div class="dropdown-toggle notification notification--bullet cursor-pointer" role="button"
                            aria-expanded="false">
                            <i data-feather="shopping-cart" class="notification__icon dark:text-gray-300"></i>
                        </div>
                        <div class="notification-content pt-2 dropdown-menu">
                            <div class="notification-content__box dropdown-menu__content box dark:bg-dark-6">
                                <div class="flex mb-6">
                                    <div class="flex-none notification-content__title text-lg">Carrinho</div>
                                    <div class="flex-grow"></div>
                                    <a class="btn btn-primary shadow-md"
                                        href="//localhost:3000/admin/products/create">Finalizar</a>
                                </div>

                                @foreach (array_slice($fakers, 0, 5) as $key => $faker)
                                    <div class="cursor-pointer relative flex items-center {{ $key ? 'mt-5' : '' }}">
                                        <div class="w-12 h-12 flex-none image-fit mr-1">
                                            <img alt="Rubick Tailwind HTML Admin Template" class="rounded-full"
                                                src="{{ asset('assets/images/' . $faker['photos'][0]) }}">
                                            <div
                                                class="w-3 h-3 bg-theme-9 absolute right-0 bottom-0 rounded-full border-2 border-white">
                                            </div>
                                        </div>
                                        <div class="ml-2 overflow-hidden">
                                            <div class="flex items-center">
                                                <a href="javascript:;"
                                                    class="font-medium truncate mr-5">{{ $faker['users'][0]['name'] }}</a>
                                                <div class="text-xs text-gray-500 ml-auto whitespace-nowrap">
                                                    {{ $faker['times'][0] }}
                                                </div>
                                            </div>
                                            <div class="w-full truncate text-gray-600 mt-0.5">
                                                {{ $faker['news'][0]['short_content'] }}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mx-auto p-4">
        <div class="flex w-full px-20px md:p-30px py-40px rounded border border-gray-300 bg-white p-6">
            <div
                class="feature-block-wrapper w-full grid grid-cols-1 gap-8 gap-y-40px md:grid-cols-2 xl:grid-cols-4 xxl:gap-30px">
                <div class="feature-block flex w-full items-start flex-row feature-block">
                    {{-- <span
                        class="flex items-center justify-center w-70px h-70px rounded-full mr-20px bg-gray-300 flex-shrink-0 text-24px font-semibold"
                        style="background-color:#feeec8">1</span> --}}
                    <div class="flex flex-col items-start">
                        <h3 class="w-full text-18px font-semibold text-gray-900 mt-0 mb-2">{{-- Your Order --}} 1. O
                            seu
                            pedido</h3>
                        <p class="w-full leading-6 text-14px">{{-- Add products to your cart, enter your details and confirm. --}}Adicione produtos ao seu carrinho,
                            introduza os seus dados e confirme.
                        </p>
                    </div>
                </div>
                <div class="feature-block flex w-full items-start flex-row feature-block">
                    {{-- <span
                        class="flex items-center justify-center w-70px h-70px rounded-full mr-20px bg-gray-300 flex-shrink-0 text-24px font-semibold"
                        style="background-color:#ceeffe">2</span> --}}
                    <div class="flex flex-col items-start">
                        <h3 class="w-full text-18px font-semibold text-gray-900 mt-0 mb-2">{{-- Picking your order --}}
                            2. Escolhendo a sua encomenda</h3>
                        <p class="w-full leading-6 text-14px">{{-- Your order is being picked and now will be forwarded for
                            packaging. --}} A sua encomenda está a ser escolhida
                            e será agora encaminhada para embalagem.</p>
                    </div>
                </div>
                <div class="feature-block flex w-full items-start flex-row feature-block">
                    {{-- <span
                        class="flex items-center justify-center w-70px h-70px rounded-full mr-20px bg-gray-300 flex-shrink-0 text-24px font-semibold"
                        style="background-color:#d4f8c4">3</span> --}}
                    <div class="flex flex-col items-start">
                        <h3 class="w-full text-18px font-semibold text-gray-900 mt-0 mb-2">{{-- Packing Your Order --}}
                            3. Embalar a sua encomenda</h3>
                        <p class="w-full leading-6 text-14px">{{-- We are packing your order and will be out for delivery
                            soon. --}} Estamos a empacotar a sua encomenda
                            e vamos sair para a entrega em breve.
                        </p>
                    </div>
                </div>
                <div class="feature-block flex w-full items-start flex-row feature-block">
                    {{-- <span
                        class="flex items-center justify-center w-70px h-70px rounded-full mr-20px bg-gray-300 flex-shrink-0 text-24px font-semibold"
                        style="background-color:#d8dafe">4</span> --}}
                    <div class="flex flex-col items-start">
                        <h3 class="w-full text-18px font-semibold text-gray-900 mt-0 mb-2">{{-- Deliver --}}
                            4. Entregar</h3>
                        <p class="w-full leading-6 text-14px">{{-- Your order has been prepared and out for delivery. It will
                            be
                            delivered soon. --}} A sua encomenda foi preparada e
                            pronta para entrega. Será
                            ser
                            entregue em breve.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Category Grid --}}


    <!-- Slider main container -->
    <div class="container mx-auto">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>
                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>

                <div class="swiper-slide p-4">
                    <div class="flex flex-col rounded shadow overflow-hidden">
                        <div class="flex-shrink-0">
                            <img class="h-48 w-full object-cover"
                                src="https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80"
                                alt="">
                        </div>
                    </div>
                </div>


            </div>
        </div>

        <!-- Add Arrows -->
        {{-- <div class="swiper-button-next swiper-button-black"></div>
    <div class="swiper-button-prev swiper-button-black"></div> --}}


        {{-- Product Grid --}}
        <div class="container mx-auto">
            <div class="relative bg-white">
                <div class="mx-auto px-4 py-10">
                    <div
                        class="grid grid-cols-2 gap-4 row-gap-6 md:grid-cols-3 md:col-gap-4 md:row-gap-8 lg:grid-cols-4 lg:col-gap-4 lg:row-gap-10 xxl:grid-cols-5 xxl:col-gap-4 xxl:row-gap-12 2xxl:grid-cols-7 2xxl:col-gap-5 2xxl:row-gap-12">
                        @foreach ($products as $product)
                            <div class="w-full flex flex-col items-start cursor-pointer">
                                <div class="flex justify-center items-center w-full rounded overflow-hidden">
                                    <img class="object-cover" src="{{ $product->imageSrc }}" alt="">
                                </div>
                                <div class="flex flex-col items-start mt-3">
                                    <span class="font-semibold text-gray-900 mb-2 text-16px">$2.5
                                    </span>
                                    <span class="text-13px">{{ $product->name }}</span>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        {{-- Footer --}}
        <div class="container mx-auto p-4">
            <div class="mx-auto">
                <footer class="footer-1 bg-gray-100 py-8 sm:py-12">
                    <div class="container mx-auto px-4">
                        <div class="sm:flex sm:flex-wrap sm:-mx-4 md:py-4">
                            <div class="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6">
                                <h5 class="text-xl font-bold mb-6">Features</h5>
                                <ul class="list-none footer-links">
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Cool
                                            stuff</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Random
                                            feature</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Team
                                            feature</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Stuff
                                            for developers</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Another
                                            one</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Last
                                            time</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 sm:mt-0">
                                <h5 class="text-xl font-bold mb-6">Resources</h5>
                                <ul class="list-none footer-links">
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Resource</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Resource
                                            name</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Another
                                            resource</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Final
                                            resource</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 md:mt-0">
                                <h5 class="text-xl font-bold mb-6">About</h5>
                                <ul class="list-none footer-links">
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Team</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Locations</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Privacy</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Terms</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="px-4 sm:w-1/2 md:w-1/4 xl:w-1/6 mt-8 md:mt-0">
                                <h5 class="text-xl font-bold mb-6">Help</h5>
                                <ul class="list-none footer-links">
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Support</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Help
                                            Center</a>
                                    </li>
                                    <li class="mb-2">
                                        <a href="#"
                                            class="border-b border-solid border-transparent hover:border-purple-800 hover:text-purple-800">Contact
                                            Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="px-4 mt-4 sm:w-1/3 xl:w-1/6 sm:mx-auto xl:mt-0 xl:ml-auto">
                                <h5 class="text-xl font-bold mb-6 sm:text-center xl:text-left">Stay connected</h5>
                                <div class="flex sm:justify-center xl:justify-start">
                                    <a href=""
                                        class="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 text-gray-600 hover:text-white hover:bg-blue-600 hover:border-blue-600">
                                        <i class="fab fa-facebook"></i>
                                    </a>
                                    <a href=""
                                        class="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 ml-2 text-gray-600 hover:text-white hover:bg-blue-400 hover:border-blue-400">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href=""
                                        class="w-8 h-8 border border-2 border-gray-400 rounded-full text-center py-1 ml-2 text-gray-600 hover:text-white hover:bg-red-600 hover:border-red-600">
                                        <i class="fab fa-google-plus-g"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="sm:flex sm:flex-wrap sm:-mx-4 mt-6 pt-6 sm:mt-12 sm:pt-12 border-t">
                            <div class="sm:w-full px-4 md:w-1/6">
                                <strong>FWR</strong>
                            </div>
                            <div class="px-4 sm:w-1/2 md:w-1/4 mt-4 md:mt-0">
                                <h6 class="font-bold mb-2">Address</h6>
                                <address class="not-italic mb-4 text-sm">
                                    123 6th St.<br>
                                    Melbourne, FL 32904
                                </address>
                            </div>
                            <div class="px-4 sm:w-1/2 md:w-1/4 mt-4 md:mt-0">
                                <h6 class="font-bold mb-2">Free Resources</h6>
                                <p class="mb-4 text-sm">Use our HTML blocks for <strong>FREE</strong>.<br>
                                    <em>All are MIT License</em>
                                </p>
                            </div>
                            <div class="px-4 md:w-1/4 md:ml-auto mt-6 sm:mt-4 md:mt-0">
                                <button class="px-4 py-2 bg-purple-800 hover:bg-purple-900 rounded text-white">Get
                                    Started</button>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>

        <script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

        <script src="{{ asset('assets/js/app.js') }}"></script>

        <script>
            var mySwiper = new Swiper('.swiper-container', {
                loop: true,
                slidesPerView: 1,
                speed: 400,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                breakpoints: {
                    640: {
                        slidesPerView: 1,
                        spaceBetween: 0,
                        autoHeight: true
                    },
                    768: {
                        slidesPerView: 2,
                        spaceBetween: 0,
                        autoHeight: true
                    },
                    1024: {
                        slidesPerView: 8,
                        spaceBetween: 0,
                        autoHeight: true
                    },
                }
            });

        </script>
</body>

</html>
