@extends('layout::side-menu')

{{-- ================================================================
  _   _                _ 
 | | | | ___  __ _  __| |
 | |_| |/ _ \/ _` |/ _` |
 |  _  |  __/ (_| | (_| |
 |_| |_|\___|\__,_|\__,_|                                     
  
================================================================= --}}

@section('subhead')
    <title>PlugCommerce | {{-- $listingInfo['PageTitle'] --}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

{{-- ================================================================
   ____            _             _   
  / ___|___  _ __ | |_ ___ _ __ | |_ 
 | |   / _ \| '_ \| __/ _ \ '_ \| __|
 | |__| (_) | | | | ||  __/ | | | |_ 
  \____\___/|_| |_|\__\___|_| |_|\__|
                                     
================================================================= --}}

@section('subcontent')
    <div class="intro-y flex flex-col sm:flex-row items-center mt-2 mb-2">
        <h2 class="text-lg font-medium mr-auto">Gestão de Páginas</h2>
        <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
            <div class="intro-y flex flex-col-reverse sm:flex-row items-center">
                <div class="w-full sm:w-auto relative mr-auto mt-3 sm:mt-0">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 ml-3 left-0 z-10 text-gray-700 dark:text-gray-300"
                        data-feather="search"></i>
                    <input type="text"
                        class="form-control w-full sm:w-64 box px-10 text-gray-700 dark:text-gray-300 placeholder-theme-13 mr-2"
                        placeholder="Procurar Produtos">
                    <div class="inbox-filter dropdown absolute inset-y-0 mr-3 right-0 flex items-center"
                        data-placement="bottom-start">
                        <i class="dropdown-toggle w-4 h-4 cursor-pointer text-gray-700 dark:text-gray-300" role="button"
                            aria-expanded="false" data-feather="chevron-down"></i>
                        <div class="inbox-filter__dropdown-menu dropdown-menu pt-2">
                            <div class="dropdown-menu__content box p-5">
                                <div class="grid grid-cols-12 gap-4 gap-y-3">
                                    <div class="col-span-6">
                                        <label for="input-filter-1" class="form-label text-xs">Nome</label>
                                        <input id="input-filter-1" type="text" class="form-control flex-1"
                                            placeholder="Insira um nome">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-2" class="form-label text-xs">SKU</label>
                                        <input id="input-filter-2" type="text" class="form-control flex-1"
                                            placeholder="Insira um sku">
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-3" class="form-label text-xs">Categoria</label>
                                        <select id="input-filter-4" class="form-select flex-1">
                                            {{-- <option>10</option>
                                            <option>25</option>
                                            <option>35</option>
                                            <option>50</option> --}}
                                        </select>
                                    </div>
                                    <div class="col-span-6">
                                        <label for="input-filter-4" class="form-label text-xs">Marca</label>
                                        <select id="input-filter-4" class="form-select flex-1">
                                            {{-- <option>10</option>
                                            <option>25</option>
                                            <option>35</option>
                                            <option>50</option> --}}
                                        </select>
                                    </div>
                                    <div class="col-span-12 flex items-center mt-3">
                                        <!--<button class="btn btn-secondary w-32 ml-auto">Create Filter</button>-->
                                        <button class="btn btn-primary w-32 ml-auto">Procurar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="btn btn-primary shadow-md" href="{{ route('plugcommerce.admin.products.create') }}">Adicionar
                Produto</a>
        </div>
    </div>
    <div class="intro-y grid grid-cols-12 gap-6 mt-4">
        <!-- BEGIN: Blog Layout -->

        @foreach ($pages as $page)
            <div class="intro-y col-span-12 md:col-span-6 xl:col-span-3 box zoom-in">
                <div class="flex items-center border-b border-gray-200 dark:border-dark-5 px-5 py-4">
                    <div class="w-10 h-10 flex-none image-fit">
                        <img alt="" class="rounded-full" src="{{ asset('assets/images/profile-15.jpg') }}">
                    </div>
                    <div class=" ml-3 mr-auto">
                        <a href="" class="font-medium">Plug With Us</a>
                        <div class="flex text-gray-600 truncate text-xs mt-0.5">
                            <a class="text-theme-1 dark:text-theme-10 inline-block truncate" href="">Administrador
                            </a> <span class="mx-1">•</span>
                            {{ $page->updated_at }}
                        </div>
                    </div>
                    <div class="dropdown ml-3">
                        <a href="javascript:;"
                            class="dropdown-toggle w-5 h-5 text-gray-600
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    dark:text-gray-300"
                            aria-expanded="false">
                            <i data-feather="more-vertical" class="w-4 h-4"></i>
                        </a>
                        <div class="dropdown-menu w-40">
                            <div class="dropdown-menu__content box dark:bg-dark-1 p-2">
                                <a href="{{ env('APP_URL') . '/' . $page->slug }}" target="blank"
                                    class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                    <i data-feather="edit-2" class="w-4 h-4 mr-2"></i>
                                    Ver Página </a>

                                <form action="{{ route('plugcommerce.admin.products.destroy', 1) }}" method="POST">
                                    <input name="_method" type="hidden" value="DELETE">
                                    @method('DELETE')
                                    @csrf()
                                    <button type="submit"
                                        class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                                        <i data-feather="trash" class="w-4 h-4 mr-2"></i>
                                        Editar Página </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-5">
                    <div class="flex intro-y gap-2 mt-4">
                        <div class="flex-none w-50">
                            <a href="" class="block font-medium text-base">{{ $page->name }} </a>
                            <div class="text-gray-700 dark:text-gray-600 mt-2">{{ $page->type }}</div>
                        </div>
                        <div class="flex-grow h-16">
                            {{-- This item will grow --}}
                        </div>
                        <div class="flex-none">
                            @if (!$page->is_home)
                                <div class="mt-2 flex">
                                    <span>{{ 'Publicada: ' }}</span>
                                    <input id="publish_checkbox_page_{{ $page->id }}" class="ml-1 form-check-switch"
                                        type="checkbox" name="layout" @if ($page->published) checked @endif onclick="publish_page({{ $page->id }})">
                                </div>
                            @endif

                            {{-- @if ($page->published)
                                <svg width="54px" height="54px" version="1.1" id="Layer_1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                    y="0px" viewBox="0 0 512 512"
                                    style="enable-background:new 0 0 512
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    512;"
                                    xml:space="preserve">
                                    <ellipse style="fill:#32BEA6;" cx="256" cy="256" rx="256" ry="255.832" />
                                    <polygon style="fill:#FFFFFF;"
                                        points="235.472,392.08 114.432,297.784 148.848,253.616 223.176,311.52 345.848,134.504
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    391.88,166.392" />
                                </svg>
                            @else
                                <svg width="54px" height="54px" version="1.1" id="Layer_1"
                                    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px"
                                    y="0px" viewBox="0 0 512 512"
                                    style="enable-background:new 0 0 512
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    512;"
                                    xml:space="preserve">
                                    <ellipse style="fill:#E04F5F;" cx="256" cy="256" rx="256" ry="255.832" />
                                    <g transform="matrix(-0.7071 0.7071 -0.7071 -0.7071 77.26 32)">
                                        <rect x="3.98" y="-427.615" style="fill:#FFFFFF;" width="55.992" height="285.672" />
                                        <rect x="-110.828" y="-312.815" style="fill:#FFFFFF;" width="285.672"
                                            height="55.992" />
                                    </g>
                                </svg>
                            @endif --}}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- BEGIN: Pagination -->
    {{-- flex flex-wrap sm:flex-row sm:flex-nowrap --}}
    {{-- <div class="intro-y col-span-12 mt-5 p-5 box">
        <div class="flex">
            <div class="flex-none w-50">
                <a class="btn btn-primary shadow-md mr-2"> <i class="w-4 h-4" data-feather="chevrons-left"></i> </a>
                <a class="btn btn-primary shadow-md mr-2"> <i class="w-4 h-4" data-feather="chevron-left"></i> </a>

            </div>
            <div class="flex-grow text-center">
                <a class="btn shadow-md ml-5" href="">1</a>
                <a class="btn btn-primary shadow-md ml-5 mr-5" href="">2</a>
                <a class="btn shadow-md mr-5" href="">3</a>
            </div>
            <div class="flex-none w-50">

                <a class="btn btn-primary shadow-md ml-2"> <i class="w-4 h-4" data-feather="chevron-right"></i></a>
                <a class="btn btn-primary shadow-md ml-2"> <i class="w-4 h-4" data-feather="chevrons-right"></i></a>

            </div>
            {{-- <select class="w-20 form-select ">
                    <option>6</option>
                    <option>12</option>
                    <option>18</option>
                    <option>24</option>
                </select> --}}
    {{-- </div>

    </div> --}}
    <!-- END: Pagination -->

    {{-- ================================================================
    __  __           _       _     
    |  \/  | ___   __| | __ _| |___ 
    | |\/| |/ _ \ / _` |/ _` | / __|
    | |  | | (_) | (_| | (_| | \__ \
    |_|  |_|\___/ \__,_|\__,_|_|___/

    ================================================================= --}}

    {{-- Modal de ativação de página --}}
    <div id="activate_modal_true" class="modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="p-5 text-center">
                        <i data-feather="x-circle" class="w-16 h-16 text-theme-6 mx-auto mt-3"></i>
                        <div class="text-3xl mt-5">Tem a certeza?</div>
                        <div class="mt-2">Pretende mesmo ativar a página? <br>Caso esteja publicada
                            irá ficar
                            online. </div>
                    </div>
                    <div class="px-5 pb-8 text-center">
                        <button type="button" data-dismiss="modal"
                            class="btn btn-outline-secondary w-24 dark:border-dark-5 dark:text-gray-300 mr-1">Cancel</button>
                        <button type="button" class="btn btn-success w-24">Activar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- ================================================================
    _   _       _   _  __ _           _   _                 
    | \ | | ___ | |_(_)/ _(_) ___ __ _| |_(_) ___  _ __  ___ 
    |  \| |/ _ \| __| | |_| |/ __/ _` | __| |/ _ \| '_ \/ __|
    | |\  | (_) | |_| |  _| | (_| (_| | |_| | (_) | | | \__ \
    |_| \_|\___/ \__|_|_| |_|\___\__,_|\__|_|\___/|_| |_|___/
                                                                    
    ================================================================= --}}

    <div id="publish-page-notification-content" class="toastify-content hidden flex">
        <i class="text-theme-9" data-feather="check-circle"></i>
        <div class="ml-4 mr-4">
            <div class="font-medium">Página publicada com Sucesso</div>
            <div class="text-gray-400 mt-1">A página irá aparecer no seu website</div>
        </div>
    </div>

    <div id="unpublish-page-notification-content" class="toastify-content hidden flex">
        <i class="text-theme-9" data-feather="check-circle"></i>
        <div class="ml-4 mr-4">
            <div class="font-medium">Página despublicada com Sucesso</div>
            <div class="text-gray-400 mt-1">A página não irá aparecer no seu website</div>
        </div>
    </div>
@endsection

{{-- ================================================================
  ____            _       _       
 / ___|  ___ _ __(_)_ __ | |_ ___ 
 \___ \ / __| '__| | '_ \| __/ __|
  ___) | (__| |  | | |_) | |_\__ \
 |____/ \___|_|  |_| .__/ \__|___/
                   |_|                    
================================================================= --}}

@section('scripts')

    {{-- para resolver o bug do carregamento, ver depois porque não funciona no app.js --}}
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>

    <script type="text/javascript">
        function publish_page(page_id) {
            if ($('#publish_checkbox_page_' + page_id).prop("checked")) {

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: 'page-publish',
                    data: {
                        page_id: page_id,
                        published: 1
                    },

                    success: function(data) {
                        console.log(data);
                        Toastify({
                            node: cash("#publish-page-notification-content")
                                .clone()
                                .removeClass("hidden")[0],
                            newWindow: true,
                            close: true,
                            gravity: "bottom",
                            position: "right",
                            stopOnFocus: true,
                            duration: 2000,
                        }).showToast();
                    }
                });

            } else {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: 'POST',
                    url: 'page-publish',
                    data: {
                        page_id: page_id,
                        published: 0
                    },

                    success: function(data) {
                        console.log(data);
                        Toastify({
                            node: cash("#unpublish-page-notification-content")
                                .clone()
                                .removeClass("hidden")[0],
                            newWindow: true,
                            close: true,
                            gravity: "bottom",
                            position: "right",
                            stopOnFocus: true,
                            duration: 2000,
                        }).showToast();
                    }
                });
            }
        }

        /*cash('#activate_modal_true').modal('show');
                
                $('#publish_checkbox_page_' + page_id).prop("checked", true);
                
            } else {
                $('#publish_checkbox_page_' + page_id).prop("checked", false);
                $.ajax({
                    type: 'POST',
                    url: 'articles/layout',
                    data: {
                        published: $('#publish_checkbox_page_' + page_id).prop("checked")
                    },

                    success: function(data) {
                        console.log(data)
                    }
                });
            }
        }*/

    </script>
@stop
