@extends('layout::side-menu')

@section('subhead')
    <title>PlugCommerce | {{-- $listingInfo['PageTitle'] --}}</title>
@endsection

@section('subcontent')
    <form method="POST" action="{{ route('plugcommerce.admin.brands.update', $brand->id) }}" @submit.prevent="onSubmit">
        @method('PUT')
        @csrf()
        <div class="intro-y flex flex-col sm:flex-row items-center mt-8">
            <h2 class="text-lg font-medium mr-auto">Editar Marca - ID: {{ $brand->id }}</h2>
            <div class="w-full sm:w-auto flex mt-4 sm:mt-0">
                <button type="submit" class="btn btn-success shadow-md mr-2">Gravar</button>
                <a class="btn btn-danger shadow-md" href="{{ route('plugcommerce.admin.brands.index') }}">Voltar</a>
            </div>
        </div>
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="intro-y col-span-12 lg:col-span-8">
                <!-- BEGIN: Input -->
                <div class="intro-y box">
                    <div class="flex flex-col sm:flex-row items-center p-5 border-b border-gray-200 dark:border-dark-5">
                        <h2 class="font-medium text-base mr-auto">Ficha de Produto</h2>
                        {{-- <div class="form-check w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0">
                    <label class="form-check-label ml-0 sm:ml-2" for="show-example-1">Show example code</label>
                    <input id="show-example-1" data-target="#input" class="show-code form-check-switch mr-0 ml-3"
                    type="checkbox">
                    </div> --}}
                    </div>
                    <div class="grid grid-cols-12 gap-6 p-5">
                        <div class="intro-y col-span-12 lg:col-span-12">
                            <!-- This is the input component -->
                            <input type="hidden" id="description" name="description" value="Teste" />
                            <div class="relative h-10 input-component mb-5">
                                <input id="name" type="text" name="name" value="{{ $brand->name }}"
                                    class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm" />
                                <label for="name" class="absolute left-2 transition-all bg-white px-1">
                                    Nome do Produto
                                </label>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="intro-y col-span-12 lg:col-span-6">
                                    <!-- This is the input component -->
                                    <div class="relative h-10 input-component mb-5">
                                        <input id="product_sku" type="text" name="product_sku" value=""
                                            class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm" />
                                        <label for="product_sku" class="absolute left-2 transition-all bg-white px-1">
                                            Referência (SKU)
                                        </label>
                                    </div>
                                </div>
                                <div class="intro-y col-span-12 lg:col-span-6">
                                    <!-- This is the input component -->
                                    <div class="relative h-10 input-component mb-5">
                                        <input id="product_price" type="text" name="product_price"
                                            class="h-full w-full border-gray-300 px-2 transition-all border-blue rounded-sm" />
                                        <label for="product_price" class="absolute left-2 transition-all bg-white px-1">
                                            Preço de Venda
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="grid grid-cols-12 gap-6">
                                <div class="intro-y col-span-12 lg:col-span-6">
                                    <!-- This is the input component -->
                                    <div class="input-group mb-5">
                                        <div class="input-group-text">Categoria</div>
                                        <select data-placeholder="Selecione uma categoria" class="tail-select w-full"
                                            name="product_category">
                                            <option value="1">Sport & Outdoor</option>
                                            <option value="2">PC & Laptop</option>
                                            <option value="3">Smartphone & Tablet</option>
                                            <option value="4">Photography</option>
                                        </select>
                                    </div>


                                </div>
                                <div class="intro-y col-span-12 lg:col-span-6">
                                    <!-- This is the input component -->
                                    <div class="input-group mb-5">
                                        <div class="input-group-text">Marca</div>
                                        <select data-placeholder="Selecione uma marca" class="tail-select w-full"
                                            name="product_brand">
                                            <option value="1" selected>Sport & Outdoor</option>
                                            <option value="2">PC & Laptop</option>
                                            <option value="3" selected>Smartphone & Tablet</option>
                                            <option value="4">Photography</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div name="product_description" data-simple-toolbar="true" class="editor">
                            </div>
                            <style>
                                label {
                                    top: 0%;
                                    transform: translateY(-50%);
                                    font-size: 11px;
                                    color: rgba(37, 99, 235, 1);
                                }

                                .empty input:not(:focus)+label {
                                    top: 50%;
                                    transform: translateY(-50%);
                                    font-size: 14px;
                                }

                                input:not(:focus)+label {
                                    color: rgba(70, 70, 70, 1);
                                }

                                input {
                                    border-width: 1px;
                                }

                                input:focus {
                                    outline: none;
                                    border-color: rgba(37, 99, 235, 1);
                                }

                            </style>
                            <script>
                                //document.getElementById('name').value = {{ $brand->name }};
                                //document.getElementById('email').value = null;
                                //document.getElementById('name').focus()
                                const allInputs = document.querySelectorAll('input');
                                for (const input of allInputs) {
                                    input.addEventListener('input', () => {
                                        const val = input.value
                                        if (!val) {
                                            input.parentElement.classList.add('empty')
                                        } else {
                                            input.parentElement.classList.remove('empty')
                                        }
                                    })
                                }

                            </script>
                        </div>
                    </div>
                </div>

            </div>
            <div class="intro-y col-span-12 lg:col-span-4">
                <!-- BEGIN: Input -->
                <div class="intro-y box">
                    <div class="col-span-12 lg:col-span-4">
                        <div class="intro-y pr-1">
                            <div class="box p-2">
                                <div class="pos__tabs nav nav-tabs justify-center" role="tablist">
                                    <a id="ticket-tab" data-toggle="tab" data-target="#ticket" href="javascript:;"
                                        class="flex-1 py-2 rounded-md text-center active" role="tab" aria-controls="ticket"
                                        aria-selected="true">Imagens</a>
                                    <!--<a id="details-tab" data-toggle="tab" data-target="#details" href="javascript:;"
                                                                                                                                                                                                                                                                                                    class="flex-1 py-2 rounded-md text-center" role="tab" aria-controls="details"
                                                                                                                                                                                                                                                                                                    aria-selected="false">Detalhes</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="tab-content">
                            <div id="ticket" class="tab-pane active" role="tabpanel" aria-labelledby="ticket-tab">
                                <div class="pos__ticket box p-2 mt-5">
                                    <div class="grid grid-cols-12 gap-6">
                                        @foreach (array_slice($fakers, 0, 3) as $faker)
                                            <div class="intro-y col-span-12 lg:col-span-4">
                                                <a href="javascript:;" data-toggle="modal" data-target="#add-item-modal"
                                                    class="">
                                                    <div class="rounded-md p-3 relative zoom-in">
                                                        <div class="flex-none pos-image relative block">
                                                            <div class="">
                                                                <img alt="Rubick Tailwind HTML Admin Template"
                                                                    src="{{ asset('assets/images/food-beverage-1.jpg') }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                {{-- <div class="box flex p-5 mt-5">
                                <div class="w-full relative text-gray-700">
                                    <input type="text"
                                        class="form-control py-3 px-4 w-full bg-gray-200 border-gray-200 pr-10 placeholder-theme-13"
                                        placeholder="Use coupon code...">
                                    <i class="w-4 h-4 hidden absolute-sm my-auto inset-y-0 mr-3 right-0"
                                        data-feather="search"></i>
                                </div>
                                <button class="btn btn-primary ml-2">Apply</button>
                            </div>
                            <div class="box p-5 mt-5">
                                <div class="flex">
                                    <div class="mr-auto">Subtotal</div>
                                    <div class="font-medium">$250</div>
                                </div>
                                <div class="flex mt-4">
                                    <div class="mr-auto">Discount</div>
                                    <div class="font-medium text-theme-6">-$20</div>
                                </div>
                                <div class="flex mt-4">
                                    <div class="mr-auto">Tax</div>
                                    <div class="font-medium">15%</div>
                                </div>
                                <div class="flex mt-4 pt-4 border-t border-gray-200 dark:border-dark-5">
                                    <div class="mr-auto font-medium text-base">Total Charge</div>
                                    <div class="font-medium text-base">$220</div>
                                </div>
                            </div>
                            <div class="flex mt-5">
                                <button
                                    class="btn w-32 border-gray-400 dark:border-dark-5 text-gray-600 dark:text-gray-300">Clear
                                    Items</button>
                                <button class="btn btn-primary w-32 shadow-md ml-auto">Charge</button>
                            </div> --}}
                            </div>
                            <div id="details" class="tab-pane" role="tabpanel" aria-labelledby="details-tab">
                                <div class="box p-5 mt-5">
                                    <div class="flex items-center border-b border-gray-200 dark:border-dark-5 pb-5">
                                        <div>
                                            <div class="text-gray-600 font-bold">Inserido por</div>
                                            <div class="mt-1">Jean-Pierre Carvalho</div>
                                        </div>
                                        <i data-feather="user" class="w-4 h-4 text-gray-600 ml-auto"></i>
                                    </div>
                                    <div class="flex items-center border-b border-gray-200 dark:border-dark-5 pt-5 pb-5">
                                        <div>
                                            <div class="text-gray-600 font-bold">Inserido em</div>
                                            <div class="mt-1">02/06/20 02:10 PM</div>
                                        </div>
                                        <i data-feather="clock" class="w-4 h-4 text-gray-600 ml-auto"></i>
                                    </div>
                                    <div class="flex items-center border-b border-gray-200 dark:border-dark-5 pt-5 pb-5">
                                        <div>
                                            <div class="text-gray-600 font-bold">Modificado por</div>
                                            <div class="mt-1">Jean-Pierre Carvalho</div>
                                        </div>
                                        <i data-feather="user" class="w-4 h-4 text-gray-600 ml-auto"></i>
                                    </div>
                                    <div class="flex items-center border-b border-gray-200 dark:border-dark-5 pt-5 pb-5">
                                        <div>
                                            <div class="text-gray-600 font-bold">Modificado em</div>
                                            <div class="mt-1">02/06/20 02:10 PM</div>
                                        </div>
                                        <i data-feather="clock" class="w-4 h-4 text-gray-600 ml-auto"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
