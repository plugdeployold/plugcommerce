<div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center mt-2">

    <a class="button text-white bg-theme-1 shadow-md mr-2" href="{{ route($create_Route) }}"
       data-toggle="modal"
       data-target="#create-modal">Adicionar novo vinho</a>

    {{--<div class="dropdown">
        <button class="dropdown-toggle button px-2 box text-gray-700 dark:text-gray-300">
                <span class="w-5 h-5 flex items-center justify-center">
                    <i class="w-4 h-4" data-feather="plus"></i>
                </span>
        </button>
         <div class="dropdown-box w-40">
             <div class="dropdown-box__content box dark:bg-dark-1 p-2">
                 <a href=""
                    class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                     <i data-feather="printer" class="w-4 h-4 mr-2"></i> Print
                 </a>
                 <a href=""
                    class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                     <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to Excel
                 </a>
                 <a href=""
                    class="flex items-center block p-2 transition duration-300 ease-in-out bg-white dark:bg-dark-1 hover:bg-gray-200 dark:hover:bg-dark-2 rounded-md">
                     <i data-feather="file-text" class="w-4 h-4 mr-2"></i> Export to PDF
                 </a>
             </div>
        </div>
    </div>--}}
    <div class="hidden md:block mx-auto text-gray-600">{{--Showing 1 to 10 of 150 entries--}}</div>
    <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
        <div class="w-56 relative text-gray-700 dark:text-gray-300">
            <input type="text" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Procurar...">
            <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
        </div>
    </div>
</div>
