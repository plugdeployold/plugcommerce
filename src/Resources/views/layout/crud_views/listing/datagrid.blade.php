
<!-- BEGIN: Data List -->
<div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <table class="table table-report -mt-2">
        <?php
            $datagrid = 'layout::crud_views.datagrids.' . $listingInfo['RoutePrefix'] .  'Datagrid';
        ?>
        @include($datagrid, ['data' => $data])
    </table>
</div>
<!-- END: Data List -->
