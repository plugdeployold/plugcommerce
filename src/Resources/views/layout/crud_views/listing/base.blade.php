

@section('subcontent')

    {{-- Variables --}}
    <?php
        $create_Route = 'stocksystem.admin.' . $listingInfo['RoutePrefix'] .  '.create';

        $top_menu_data = [
           'create_Route' => $create_Route
        ];
    ?>

    {{-- Page Title --}}
    <h2 class="intro-y text-lg font-medium mt-10">{{ $listingInfo['PageTitle'] }}</h2>

    <div class="grid grid-cols-12 gap-6 mt-5">

        {{-- Top Menu --}}
        @include('layout::crud_views.listing.top-menu', $top_menu_data)

        {{-- Datagrid --}}
        @include('layout::crud_views.listing.datagrid', ['data' => $data, 'listingInfo' => $listingInfo])

        {{-- Pagination --}}
        @include('layout::crud_views.listing.pagination')

    </div>
    <!-- BEGIN: Delete Confirmation Modal -->


@endsection
