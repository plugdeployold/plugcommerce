<thead>
<tr>

</tr>
</thead>
<tbody>
@foreach ($data as $data_row)
    <tr class="intro-x">
        <td>
            <a href="" class="font-medium whitespace-nowrap">{{ $data_row->id  }}</a>
            <div
                class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ $data_row->id }}</div>
        </td>

        </td>
        <td>
            <div class="grid grid-cols-12">
                <div class="col-span-2">
                    <img alt="" class="tooltip rounded-full w-12"
                         src="https://w7.pngwing.com/pngs/786/279/png-transparent-male-portrait-illustration-twitch-emote-internet-meme-team-fortress-2-others-thumbnail.png"
                         title="">
                </div>
                <div class="col-span-10">
                    <a href="" class="font-medium whitespace-nowrap">{{ $data_row->name  }}</a>
                    <div
                        class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ $data_row->name }}</div>
                </div>
            </div>
        </td>
        <td>
            <a {{--href="{{ route('stocksystem.admin.wines.show', $data_row->id) }}" --}}>Ver
                Sprints</a>
            <div class="text-gray-600 text-xs whitespace-nowrap mt-0.5">{{ $data_row->name }}</div>
        </td>
        <td class="">{{ $data_row->name }}</td>
        <td class="w-40">
            <div
                class="flex items-center {{ $data_row->status ? 'text-theme-9' : 'text-theme-6' }}">
                <i data-feather="{{ $data_row->status ? 'check-square' : 'x-square' }}"
                   class="w-4 h-4 mr-2"></i> {{ $data_row->status ? 'Active' : 'Inactive' }}
            </div>
        </td>
        <td class="table-report__action w-56">
            <div class="flex  items-center">
                <a class="flex items-center mr-3"
                {{--href="{{ route('stocksystem.admin.wines.edit', $data_row->id) }}" --}}>
                 <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit
             </a>
             <form method="POST" {{--action="{{ route('stocksystem.admin.wines.destroy', $wine->id) }}"--}}
                   @submit.prevent="onSubmit">
                 @csrf()
                 @method('DELETE')
                 <input type="hidden" name="id" value="{{ $data_row->id }}"/>
                 <button type="submit" class="flex items-center text-theme-6">
                     <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Apagar
                 </button>
             </form>
             {{--<a class="flex items-center text-theme-6" href="javascript:;" data-toggle="modal"
                data-target="#delete-confirmation-modal">
                 <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Delete
             </a>--}}
            </div>
        </td>
    </tr>
@endforeach
</tbody>
