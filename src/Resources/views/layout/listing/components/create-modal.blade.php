<div class="modal" id="create-modal">
    <form method="{{ $form['method'] }}"
          action="{{ ($form['params']) ? route($form['action'], $form['params']) : route($form['action']) }}"
          @submit.prevent="onSubmit">
        @csrf()
        <div class="modal__content relative">
            @if ($components['closeCross']['status'])
                <a data-dismiss="modal" href="javascript:;" class="absolute right-0 top-0 mt-3 mr-3"> <i
                        data-feather="x" class="w-8 h-8 text-gray-500"></i> </a>
            @endif
            <div class="p-5 text-center">
                <i data-feather="edit" class="w-16 h-16 text-theme-41 mx-auto mt-3"></i>
                <div class="text-3xl mt-5">{{ $text['mainTitle'] }}</div>
                <div class="text-gray-600 mt-2">{{ $text['mainDescription'] }}
                </div>
                <div class="mt-10 text-left">

                    @if (isset($components['fields']))
                        @foreach ($components['fields'] as $field)
                            @if (isset($field))
                                @switch($field['fieldType'])
                                    @case('input')
                                    <label
                                        class="block text-xs font-semibold text-gray-500 mb-2">{{ $field['label'] }}</label>
                                    <input
                                        class="w-full px-3 py-2 mb-5 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors"
                                        placeholder="{{ $field['placeholder'] }}" name="{{ $field['name'] }}"/>
                                    @break

                                    @case('select')
                                    <label class="block text-xs font-semibold text-gray-500 mb-2">{{ $field['label'] }}</label>
                                    <select class="w-full px-3 py-2 mb-5 border-2 border-gray-200 rounded-md focus:outline-none focus:border-indigo-500 transition-colors" name="{{ $field['name'] }}">
                                        @if (isset($field['options']))
                                            @foreach ($field['options'] as $option)
                                                <option value="{{ $option['value'] }}"> {{ $option['name'] }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    @break
                                @endswitch
                            @endif
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="px-5 pb-8 text-center">
                <button type="button" data-dismiss="modal"
                        class="button w-24 bg-theme-6 text-white">{{ $text['btnCancel'] }}</button>
                <button type="submit" class="button w-48 bg-theme-41 text-white">{{ $text['btnAdd'] }}</button>
            </div>
        </div>
    </form>
</div>
