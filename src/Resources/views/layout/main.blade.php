@extends('layout::base')

@section('body')

    <body class="app">
        @yield('content')
        {{-- @include('customerbackoffice::layout.components.dark-mode-switcher') --}}

        <!-- BEGIN: JS Assets-->
        <!--<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>-->
        <!--<script src="https://maps.googleapis.com/maps/api/js?key=[" your-google-map-api"]&libraries=places"></script>-->

        <!-- <script src="/plugcommerce/admin/default/assets/js/app.js"></script> -->
        {{-- <!--<script src="{{ mix('dist/js/app.js') }}"></script>--> --}}
        <!-- END: JS Assets-->

        <script src="{{ asset('dist/js/app.js') }}"></script>

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

        @yield('scripts')
    </body>
@endsection
