<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{ 'dark' }}" {{-- class="{{ $dark_mode ? 'dark' : '' --}}
    {{-- the-light --}}>

<head>
    <title>Plug With Us - Platform</title>
    <meta charset=" utf-8">
    <link href="{{ asset('assets/images/logo.svg') }}" rel="shortcut icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="LEFT4CODE">

    @yield('head')

    <!-- BEGIN: CSS Assets-->
    <!--<link rel="stylesheet" href="/dist/css/application.css" />-->
    <link rel="stylesheet" href="{{ asset('dist/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('dist/css/_tailwind.css') }}" />


    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">


    <!--<link rel="stylesheet" href="/plugcommerce/admin/default/assets/css/app.css" />-->
    {{-- <!--<link rel="stylesheet" href="{{ mix('dist/css/application.css') }}" />
    <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" />-->
    <!-- END: CSS Assets--> --}}
</head>
<!-- END: Head -->

@yield('body')

</html>
