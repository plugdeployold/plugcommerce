<?php

return [
    'sidebar' => [
        'dashboard' => 'Painel de Controlo',
        'wine' => 'Vinhos',
    ],
    'top-bar' => [
        'notifications' => 'Notificações',
        'profile' => 'Perfil',
        'reset_password' => 'Modificar Password',
        'help' => 'Ajuda',
        'logout' => 'Sair'
    ],
    'wines' => [
        'title' => 'Vinhos',
        'add-btn-title' => 'Adicionar Vinho',
        'add-title' => 'Adicionar Vinho',
        'save-btn-title' => 'Guardar Vinho',
        'requiredInfo' => 'Informações Obrigatórias',
        'opcionalInfo' => 'Informações Opcionais',
        'model' => [
            'name' => 'Nome',
            'description' => 'Descrição',
            'harvest_year' => 'Ano de colheita',
            'bottling_year' => 'Ano de engarrafamento',
            'temp_min' => 'Temperatura mínima',
            'temp_max' => 'Temperatura máxima',
            'alcohol_percentage' => 'Percentagem de alcool',
            'time_wine_stage' => 'Tempo de Estágio',
            'id_region' => 'Região',
            'id_producer' => 'Produtor',
            'id_wine_farm' => 'Quinta',
            'id_country' => 'País',
        ],
    ],
    'bottles' => [
        'title' => 'Garrafas',
        'add-btn-title' => 'Adicionar Garrafa',
    ],
    'wine_types' => [
        'title' => 'Tipos de Vinhos',
        'add-btn-title' => 'Adicionar Tipo de Vinho',
    ],
    'wine_grapes' => [
        'title' => 'Castas',
        'add-btn-title' => 'Adicionar Casta',
    ],
    'bottle_capacities' => [
        'title' => 'Capacidade das Garrafas',
        'add-btn-title' => 'Adicionar Capacidade',
    ],
    'wine_photos' => [
        'title' => 'Fotos dos Vinhos',
        'add-btn-title' => 'Adicionar Foto do Vinho',
    ],
    'regions' => [
        'title' => 'Regiões',
        'add-btn-title' => 'Adicionar Região',
    ],
    'wine_makers' => [
        'title' => 'Enólogos',
        'add-btn-title' => 'Adicionar Enólogo',
    ],
    'producers' => [
        'title' => 'Produtores',
        'add-btn-title' => 'Adicionar Produtor',
    ],
    'wine_farms' => [
        'title' => 'Quintas',
        'add-btn-title' => 'Adicionar Quinta',
    ],
    'wine_states' => [
        'title' => 'Estados de Vinho',
        'add-btn-title' => 'Adicionar Estado de Vinho',
    ],
    'wine_cellars' => [
        'title' => 'Garrafeiras',
        'add-btn-title' => 'Adicionar Garrafeira',
    ],
    'wine_cellar_sections' => [
        'title' => 'Secções da Garrafeira',
        'add-btn-title' => 'Adicionar Secção da Garrafeira',
    ],
    'wine_tasting_notes' => [
        'title' => 'Notas de Prova',
        'add-btn-title' => 'Adicionar Nota de Prova',
    ],
    'activities' => [
        'title' => 'Actividades',
        'add-btn-title' => 'Adicionar Actividade',
    ],
];
