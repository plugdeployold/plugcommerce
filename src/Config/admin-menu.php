<?php

return [
    [
        'key' => 'stocksystem',
        'name' => 'Wine Cellar',
        'route' => 'stocksystem.admin.index',
        'sort' => 1,
        'icon-class' => 'dashboard-icon',
    ],
    [
        'key' => 'stocksystem.wines',
        'name' => 'stocksystem::app.wines.title',
        'route' => 'stocksystem.admin.wines.index',
        'sort' => 1,
        'icon-class' => 'dashboard-icon',
    ], [
        'key' => 'stocksystem.bottles',
        'name' => 'stocksystem::app.bottles.title',
        'route' => 'stocksystem.admin.bottles.index',
        'sort' => 2,
        'icon-class' => 'dashboard-icon',
    ], [
        'key' => 'stocksystem.wine_types',
        'name' => 'stocksystem::app.wine_types.title',
        'route' => 'stocksystem.admin.wine_types.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_grapes',
        'name' => 'stocksystem::app.wine_grapes.title',
        'route' => 'stocksystem.admin.wine_grapes.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.bottle_capacities',
        'name' => 'stocksystem::app.bottle_capacities.title',
        'route' => 'stocksystem.admin.bottle_capacities.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_photos',
        'name' => 'stocksystem::app.wine_photos.title',
        'route' => 'stocksystem.admin.wine_photos.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.regions',
        'name' => 'stocksystem::app.regions.title',
        'route' => 'stocksystem.admin.regions.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_makers',
        'name' => 'stocksystem::app.wine_makers.title',
        'route' => 'stocksystem.admin.wine_makers.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.producers',
        'name' => 'stocksystem::app.producers.title',
        'route' => 'stocksystem.admin.producers.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_farms',
        'name' => 'stocksystem::app.wine_farms.title',
        'route' => 'stocksystem.admin.wine_farms.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_states',
        'name' => 'stocksystem::app.wine_states.title',
        'route' => 'stocksystem.admin.wine_states.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_cellars',
        'name' => 'stocksystem::app.wine_cellars.title',
        'route' => 'stocksystem.admin.wine_cellars.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_cellar_sections',
        'name' => 'stocksystem::app.wine_cellar_sections.title',
        'route' => 'stocksystem.admin.wine_cellar_sections.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.wine_tasting_notes',
        'name' => 'stocksystem::app.wine_tasting_notes.title',
        'route' => 'stocksystem.admin.wine_tasting_notes.index',
        'sort' => 1,
        'icon-class' => '',
    ], [
        'key' => 'stocksystem.activities',
        'name' => 'stocksystem::app.activities.title',
        'route' => 'stocksystem.admin.activities.index',
        'sort' => 1,
        'icon-class' => '',
    ],

];
