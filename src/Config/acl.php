<?php

return [
    [
        'key' => 'stocksystem',
        'name' => 'StockSystem',
        'route' => 'stocksystem.admin.index',
        'sort' => 2,
    ],
];
