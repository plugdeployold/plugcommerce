<?php

return [
    [
        'key' => 'wine-cellar',
        'name' => 'Wine Cellar',
        'sort' => 1,
    ], [
        'key' => 'wine-cellar.settings',
        'name' => 'Custom Settings',
        'sort' => 1,
    ], [
        'key' => 'wine-cellar.settings.settings',
        'name' => 'Custom Groupings',
        'sort' => 1,
        'fields' => [
            [
                'name' => 'status',
                'title' => 'Status',
                'type' => 'boolean',
                'channel_based' => true,
                'locale_based' => false,
            ],
        ],
    ],
];
