<?php

Route::group(['middleware' => ['web'], 'prefix' => '/admin/stocksystem',], function () {

//  WINE
    Route::group(['namespace' => 'WineCellar\StockSystem\Http\Controllers\Admin\Wine', 'as' => 'stocksystem.admin.'], function () {

        Route::resource('wine', WineController::class)->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]);

        /**************************************************************************************************************/
        /* Wine Routes */
        /**************************************************************************************************************/
        Route::get('/wine/findAll', 'WineController@findAll');

        Route::resource('wine', WineController::class)->only([
            'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
        ]);
    });
});
