<?php

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product\ProductController as ProductController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category\CategoryController as CategoryController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Brand\BrandController as BrandController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Attribute\AttributeController as AttributeController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Order\OrderController as OrderController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Customer\CustomerController as CustomerController;

// Models Plug Wine - 08-04-2021 - Jean-Pierre Carvalho
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Wine\WineController as WineController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Bottle\BottleController as BottleController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineType\WineTypeController as WineTypeController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineGrape\WineGrapeController as WineGrapeController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\BottleCapacity\BottleCapacityController as BottleCapacityController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WinePhoto\WinePhotoController as WinePhotoController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Region\RegionController as RegionController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineMaker\WineMakerController as WineMakerController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Producer\ProducerController as ProducerController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineState\WineStateController as WineStateController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellar\WineCellarController as WineCellarController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellarSection\WineCellarSectionController as WineCellarSectionController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineTastingNote\WineTastingNoteController as WineTastingNoteController;
use PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Activity\ActivityController as ActivityController;

Route::group(['middleware' => ['web'], 'prefix'=>'/admin',  'name' => 'admin'], function () {

    Route::get('/', function () {
        return "admin";
    });

    Route::get('/dashboard', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\DashboardMenuController@index')
    ->name('plugcommerce.admin.dashboard');

    // Admin Products Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::resource('products', ProductController::class, ['names' => 'plugcommerce.admin.products'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Categories Routes - 29-03-2021 - Jean-Pierre Carvalho
    Route::resource('categories', CategoryController::class, ['names' => 'plugcommerce.admin.categories'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Brands Routes - 29-03-2021 - Jean-Pierre Carvalho
    Route::resource('brands', BrandController::class, ['names' => 'plugcommerce.admin.brands'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Attributes Routes - 29-03-2021 - Jean-Pierre Carvalho
    Route::resource('attributes', AttributeController::class, ['names' => 'plugcommerce.admin.attributes'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Products Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/products-menu',], function () {

        Route::get('/products-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@products_manager')
        ->name('plugcommerce.admin.products.manager');

        Route::get('/categories-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@categories_manager')
        ->name('plugcommerce.admin.categories.manager');

        Route::get('/brands-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@brands_manager')
        ->name('plugcommerce.admin.brands.manager');

        Route::get('/attributes-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@attributes_manager')
        ->name('plugcommerce.admin.attributes.manager');

        Route::get('/import', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@import')
        ->name('plugcommerce.admin.products.import');

        Route::get('/export', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\ProductMenuController@export')
        ->name('plugcommerce.admin.products.export');
    });

    // Admin Orders Routes - 29-03-2021 - Jean-Pierre Carvalho
    Route::resource('orders', OrderController::class, ['names' => 'plugcommerce.admin.orders'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Orders Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/orders-menu',], function () {

        Route::get('/orders-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\OrdersMenuController@orders_manager')->name('plugcommerce.admin.orders.orders_manager');

        Route::get('/shipping-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\OrdersMenuController@shipping_manager')->name('plugcommerce.admin.orders.shipping_manager');

        Route::get('/invoices-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\OrdersMenuController@invoices_manager')->name('plugcommerce.admin.orders.invoices_manager');

        Route::get('/refunds-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\OrdersMenuController@refunds_manager')->name('plugcommerce.admin.orders.refunds_manager');
        
        Route::get('/automatic-invoices', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\OrdersMenuController@automatic_invoices')->name('plugcommerce.admin.orders.automatic_invoices');
    
    });

    // Admin Customers Routes - 29-03-2021 - Jean-Pierre Carvalho
    Route::resource('customers', CustomerController::class, ['names' => 'plugcommerce.admin.customers'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Customers Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/customers-menu',], function () {

        Route::get('/customers-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CustomersMenuController@customers_manager')
        ->name('plugcommerce.admin.customers.customers_manager');

        Route::get('/groups-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CustomersMenuController@groups_manager')
        ->name('plugcommerce.admin.customers.groups_manager');

        Route::get('/reviews-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CustomersMenuController@reviews_manager')
        ->name('plugcommerce.admin.customers.reviews_manager');

        Route::get('/import', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CustomersMenuController@import')
        ->name('plugcommerce.admin.customers.import');

        Route::get('/export', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CustomersMenuController@export')
        ->name('plugcommerce.admin.customers.export');
    });

    // Admin Cms Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/cms',], function () {

        Route::get('/pages', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CMSMenuController@pages_manager')
        ->name('plugcommerce.admin.cms-menu.pages');

        Route::post('/page-publish', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\CMS\PageController@publish')
        ->name('plugwine.admin.dashboard.index');

        Route::get('/translations', 'PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu\CMSMenuController@translations_manager')
        ->name('plugcommerce.admin.cms-menu.translations');
    });  

   

    // Admin Layout Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/layout',], function () {
        Route::get('/settings', function () {
            return view('plugcommerce::layout.settings');
        })->name('plugcommerce.admin.layout.settings');

        Route::get('/theme', function () {
            return view('plugcommerce::layout.theme');
        })->name('plugcommerce.admin.layout.theme');

        Route::get('/banners', function () {
            return view('plugcommerce::layout.banners');
        })->name('plugcommerce.admin.layout.banners');

        Route::get('/emails', function () {
            return view('plugcommerce::layout.emails');
        })->name('plugcommerce.admin.layout.emails');

        Route::get('/popups', function () {
            return view('plugcommerce::layout.popups');
        })->name('plugcommerce.admin.layout.popups');

        Route::get('/forms', function () {
            return view('plugcommerce::layout.forms');
        })->name('plugcommerce.admin.layout.forms');
    });  

    // Admin Marketing Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/marketing',], function () {
        Route::get('/cupons', function () {
            return view('plugcommerce::marketing.cupons');
        })->name('plugcommerce.admin.marketing.cupons');

        Route::get('/pixels', function () {
            return view('plugcommerce::marketing.pixels');
        })->name('plugcommerce.admin.marketing.pixels');

        Route::get('/social', function () {
            return view('plugcommerce::marketing.social');
        })->name('plugcommerce.admin.marketing.social');

        Route::get('/channels', function () {
            return view('plugcommerce::marketing.channels');
        })->name('plugcommerce.admin.marketing.channels');

        Route::get('/newsletters', function () {
            return view('plugcommerce::marketing.newsletters');
        })->name('plugcommerce.admin.marketing.newsletters');

        Route::get('/seo', function () {
            return view('plugcommerce::marketing.seo');
        })->name('plugcommerce.admin.marketing.seo');
    });  

    // Admin Settings Menu Routes - 28-03-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/settings',], function () {
        Route::get('/payments', function () {
            return view('plugcommerce::settings.payments');
        })->name('plugcommerce.admin.settings.payments');

        Route::get('/languages', function () {
            return view('plugcommerce::settings.languages');
        })->name('plugcommerce.admin.settings.languages');

        Route::get('/currencies', function () {
            return view('plugcommerce::settings.currencies');
        })->name('plugcommerce.admin.settings.currencies');

        Route::get('/domains', function () {
            return view('plugcommerce::settings.domains');
        })->name('plugcommerce.admin.settings.domains');

        Route::get('/carriers', function () {
            return view('plugcommerce::settings.carriers');
        })->name('plugcommerce.admin.settings.carriers');

        Route::get('/taxes', function () {
            return view('plugcommerce::settings.taxes');
        })->name('plugcommerce.admin.settings.taxes');
    });  

    Route::resource('wine', WineController::class, ['names' => 'plugwine.admin.wine'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('bottle', BottleController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-type', WineTypeController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-grape', WineGrapeController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('bottle-capacity', BottleCapacityController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-photo', WinePhotoController::class, ['names' => 'plugwine.admin.wine-photos'])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('region', RegionController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-maker', WineMakerController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('producer', ProducerController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-state', WineStateController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-cellar', WineCellarController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-cellar-section', WineCellarSectionController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('wine-tasting-note', WineTastingNoteController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    Route::resource('activity', ActivityController::class, ['names' => ' '])->only([
        'index', 'create', 'store', 'show', 'edit', 'update', 'destroy'
    ]);

    // Admin Plug Wine - 08-04-2021 - Jean-Pierre Carvalho
    Route::group(['middleware' => ['web'], 'prefix'=>'/plugwine',], function () {

        Route::get('/', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@dashboard')
        ->name('plugwine.admin.dashboard.index');

        Route::get('/wines-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wines_manager')
        ->name('plugcommerce.admin.plugwine.wines_manager');

        Route::get('/bottles-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@bottles_manager')
        ->name('plugcommerce.admin.plugwine.bottles_manager');

        Route::get('/wine-types-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_types_manager')
        ->name('plugcommerce.admin.plugwine.wine_types_manager');

        Route::get('/wine-grapes-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_grapes_manager')
        ->name('plugcommerce.admin.plugwine.wine_grapes_manager');

        Route::get('/bottle-capacities-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@bottle_capacities_manager')
        ->name('plugcommerce.admin.plugwine.bottle_capacities_manager');

        Route::get('/wine-photos-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_photos_manager')
        ->name('plugcommerce.admin.plugwine.wine_photos_manager');

        Route::get('/regions-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@regions_manager')
        ->name('plugcommerce.admin.plugwine.regions_manager');

        Route::get('/wine-makers-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_makers_manager')
        ->name('plugcommerce.admin.plugwine.wine_makers_manager');

        Route::get('/producers-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@producers_manager')
        ->name('plugcommerce.admin.plugwine.producers_manager');

        Route::get('/wine-farms-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_farms_manager')
        ->name('plugcommerce.admin.plugwine.wine_farms_manager');

        Route::get('/wine-states-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_states_manager')
        ->name('plugcommerce.admin.plugwine.wine_states_manager');

        Route::get('/wine-cellars-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_cellars_manager')
        ->name('plugcommerce.admin.plugwine.wine_cellars_manager');

        Route::get('/wine-cellar-sections-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_cellar_sections_manager')
        ->name('plugcommerce.admin.plugwine.wine_cellar_sections_manager');

        Route::get('/wine-tasting-notes-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@wine_tasting_notes_manager')
        ->name('plugcommerce.admin.plugwine.wine_tasting_notes_manager');

        Route::get('/activities-manager', 'PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\MenuController@activities_manager')
        ->name('plugcommerce.admin.plugwine.activities_manager');
    });

});
