<?php

Route::group(['middleware' => ['web'], 'prefix'=>'/shop',  'name' => 'shop'], function () {

    Route::get('/', 'PlugDeploy\PlugCommerce\Http\Controllers\Shop\ShopController@shop_product_grid')
    ->name('plugcommerce.shop.product-grid');

});

?>