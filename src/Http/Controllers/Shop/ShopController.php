<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Shop;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product\ProductController as ProductController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category\CategoryController as CategoryController;

class ShopController
{
    public function __construct()
    {
        
    }

    // ✅ Product Index With All Products
    public function shop_product_grid()
    {
        $products = \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product\ProductController@findAll');
        $categories =  \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category\CategoryController@index');
    
        return view('shop::themes.default.pages.product-grid', compact('products', 'categories'));
    }
            
}

