<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine;

class MenuController
{
    public function __construct()
    {
        
    }

    public function dashboard()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineController@index');
    }

    public function wines_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Wine\WineController@index');
    }

    public function bottles_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Bottle\BottleController@index');
    }

    public function wine_types_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineType\WineTypeController@index');
    }

    public function wine_grapes_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineGrape\WineGrapeController@index');
    }

    public function bottle_capacities_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\BottleCapacity\BottleCapacityController@index');
    }
    
    public function wine_photos_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WinePhoto\WinePhotoController@index');
    }

    public function regions_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Region\RegionController@index');
    }

    public function wine_makers_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineMaker\WineMakerController@index');
    }

    public function producers_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Producer\ProducerController@index');
    }

    public function wine_farms_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineFarm\WineFarmController@index');
    }

    public function wine_states_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineState\WineStateController@index');
    }

    public function wine_cellars_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellar\WineCellarController@index');
    }

    public function wine_cellar_sections_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellarSection\WineCellarSectionController@index');
    }

    public function wine_tasting_notes_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineTastingNote\WineTastingNoteController@index');
    }

    public function activities_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Activity\ActivityController@index');
    }
}

