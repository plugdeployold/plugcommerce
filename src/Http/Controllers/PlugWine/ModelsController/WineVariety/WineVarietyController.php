<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineVariety;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineVariety\WineVarietyRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineVariety;

class WineVarietyController
{
    protected $response, $repository;

    public function __construct(WineVarietyRepository $repository)
    {
        $this->route_prefix = 'wine-variety';

        $this->wineVariety = new WineVariety();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineVarieties  = $this->repository->findAll();
        
        return view('plugwine::wine-variety.index', compact('wineVarieties'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-variety.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineVariety = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-variety.edit', ['wineVariety' => $wineVariety->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineVariety = $this->repository->findOneById($id);
 
         return view('plugwine::wine-variety.edit', compact('wineVariety'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-variety.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-variety.index');
    }
}

