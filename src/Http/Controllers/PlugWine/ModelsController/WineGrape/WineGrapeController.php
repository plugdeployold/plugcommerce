<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineGrape;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineGrape\WineGrapeRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineGrape;

class WineGrapeController
{
    protected $response, $repository;

    public function __construct(WineGrapeRepository $repository)
    {
        $this->route_prefix = 'wine-grape';

        $this->wineGrape = new WineGrape();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineGrapes  = $this->repository->findAll();
        
        return view('plugwine::wine-grape.index', compact('wineGrapes'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-grape.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineGrape = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-grape.edit', ['wineGrape' => $wineGrape->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineGrape = $this->repository->findOneById($id);
 
         return view('plugwine::wine-grape.edit', compact('wineGrape'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-grape.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-grape.index');
    }
}

