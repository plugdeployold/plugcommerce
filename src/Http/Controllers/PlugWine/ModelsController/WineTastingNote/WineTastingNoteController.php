<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineTastingNote;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineTastingNote\WineTastingNoteRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineTastingNote;

class WineTastingNoteController
{
    protected $response, $repository;

    public function __construct(WineTastingNoteRepository $repository)
    {
        $this->route_prefix = 'wine-tasting-note';

        $this->wineTastingNote = new WineTastingNote();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineTastingNotes  = $this->repository->findAll();
        
        return view('plugwine::wine-tasting-note.index', compact('wineTastingNotes'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-tasting-note.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineTastingNote = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-tasting-note.edit', ['wineTastingNote' => $wineTastingNote->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineTastingNote = $this->repository->findOneById($id);
 
         return view('plugwine::wine-tasting-note.edit', compact('wineTastingNote'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-tasting-note.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-tasting-note.index');
    }
}

