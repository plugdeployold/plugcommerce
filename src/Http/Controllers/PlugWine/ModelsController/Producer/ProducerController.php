<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Producer;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Producer\ProducerRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Producer;

class ProducerController
{
    protected $response, $repository;

    public function __construct(ProducerRepository $repository)
    {
        $this->route_prefix = 'producer';

        $this->producer = new Producer();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $producers  = $this->repository->findAll();
        
        return view('plugwine::producer.index', compact('producers'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::producer.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $producer = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::producer.edit', ['producer' => $producer->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $producer = $this->repository->findOneById($id);
 
         return view('plugwine::producer.edit', compact('producer'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::producer.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::producer.index');
    }
}

