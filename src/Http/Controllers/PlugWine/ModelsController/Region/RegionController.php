<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Region;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Region\RegionRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Region;

class RegionController
{
    protected $response, $repository;

    public function __construct(RegionRepository $repository)
    {
        $this->route_prefix = 'region';

        $this->region = new Region();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $regions  = $this->repository->findAll();
        
        return view('plugwine::region.index', compact('regions'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::region.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $region = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::region.edit', ['region' => $region->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $region = $this->repository->findOneById($id);
 
         return view('plugwine::region.edit', compact('region'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::region.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::region.index');
    }
}

