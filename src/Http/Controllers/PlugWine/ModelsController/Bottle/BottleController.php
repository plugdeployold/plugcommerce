<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Bottle;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Bottle\BottleRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Bottle;

class BottleController
{
    protected $response, $repository;

    public function __construct(BottleRepository $repository)
    {
        $this->route_prefix = 'Bottle';

        $this->bottle = new Bottle();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $bottles  = $this->repository->findAll();
        
        return view('plugwine::bottle.index', compact('bottles'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::bottle.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $bottle = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::bottle.edit', ['bottle' => $bottle->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $bottle = $this->repository->findOneById($id);
 
         return view('plugwine::bottle.edit', compact('bottle'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::bottle.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::bottle.index');
    }
}

