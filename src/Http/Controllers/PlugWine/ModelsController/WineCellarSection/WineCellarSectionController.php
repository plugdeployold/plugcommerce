<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellarSection;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineCellarSection\WineCellarSectionRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineCellarSection;

class WineCellarSectionController
{
    protected $response, $repository;

    public function __construct(WineCellarSectionRepository $repository)
    {
        $this->route_prefix = 'wine-cellar-section';

        $this->wineCellarSection = new WineCellarSection();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineCellarSections  = $this->repository->findAll();
        
        return view('plugwine::wine-cellar-section.index', compact('wineCellarSections'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-cellar-section.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineCellarSection = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-cellar-section.edit', ['wineCellarSection' => $wineCellarSection->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineCellarSection = $this->repository->findOneById($id);
 
         return view('plugwine::wine-cellar-section.edit', compact('wineCellarSection'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-cellar-section.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-cellar-section.index');
    }
}

