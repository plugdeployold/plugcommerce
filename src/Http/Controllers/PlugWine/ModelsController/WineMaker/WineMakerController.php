<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineMaker;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineMaker\WineMakerRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineMaker;

class WineMakerController
{
    protected $response, $repository;

    public function __construct(WineMakerRepository $repository)
    {
        $this->route_prefix = 'wine-maker';

        $this->wineMaker = new WineMaker();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineMakers  = $this->repository->findAll();
        
        return view('plugwine::wine-maker.index', compact('wineMakers'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-maker.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineMaker = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-maker.edit', ['wineMaker' => $wineMaker->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineMaker = $this->repository->findOneById($id);
 
         return view('plugwine::wine-maker.edit', compact('wineMaker'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-maker.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-maker.index');
    }
}

