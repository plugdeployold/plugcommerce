<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WinePhoto;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WinePhoto\WinePhotoRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WinePhoto;

use Illuminate\Http\Request;

class WinePhotoController
{
    protected $response, $repository;

    public function __construct(WinePhotoRepository $repository)
    {
        $this->route_prefix = 'wine-photo';

        $this->winePhoto = new WinePhoto();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $winePhotos  = $this->repository->findAll();
        
        return view('plugwine::wine-photo.index', compact('winePhotos'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-photo.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store(Request $request)
     {
        $path = $request->file('image')->store('wine_photos', 's3');

        $wine = [
            'name' => time(). '_' . $request->name,
            'url' => 'https://plugwine-vlb-group-2021.s3.amazonaws.com/' . $path
        ];

        $winePhoto = $this->repository->store($wine);

        return $winePhoto;
 
         //return redirect()->route('plugwine::wine-photo.edit', ['winePhoto' => $winePhoto->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $winePhoto = $this->repository->findOneById($id);
 
         return view('plugwine::wine-photo.edit', compact('winePhoto'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-photo.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-photo.index');
    }
}

