<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineFarm;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineFarm\WineFarmRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineFarm;

class WineFarmController
{
    protected $response, $repository;

    public function __construct(WineFarmRepository $repository)
    {
        $this->route_prefix = 'wine-farm';

        $this->wineFarm = new WineFarm();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineFarms  = $this->repository->findAll();
        
        return view('plugwine::wine-farm.index', compact('wineFarms'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-farm.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineFarm = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-farm.edit', ['wineFarm' => $wineFarm->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineFarm = $this->repository->findOneById($id);
 
         return view('plugwine::wine-farm.edit', compact('wineFarm'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-farm.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-farm.index');
    }
}

