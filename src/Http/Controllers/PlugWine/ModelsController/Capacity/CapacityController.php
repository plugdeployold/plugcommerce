<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Capacity;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Capacity\CapacityRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Capacity;

class CapacityController
{
    protected $response, $repository;

    public function __construct(CapacityRepository $repository)
    {
        $this->route_prefix = 'capacity';

        $this->capacity = new Capacity();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $capacities  = $this->repository->findAll();
        
        return view('plugwine::capacities.index', compact('capacities'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::capacities.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $capacity = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::capacities.edit', ['capacity' => $capacity->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $capacity = $this->repository->findOneById($id);
 
         return view('plugwine::capacities.edit', compact('capacity'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::capacities.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::capacities.index');
    }
}

