<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineCellar;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineCellar\WineCellarRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineCellar;

class WineCellarController
{
    protected $response, $repository;

    public function __construct(WineCellarRepository $repository)
    {
        $this->route_prefix = 'wine-cellar';

        $this->wineCellar = new WineCellar();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineCellars  = $this->repository->findAll();
        
        return view('plugwine::wine-cellar.index', compact('wineCellars'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-cellar.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineCellar = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-cellar.edit', ['wineCellar' => $wineCellar->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineCellar = $this->repository->findOneById($id);
 
         return view('plugwine::wine-cellar.edit', compact('wineCellar'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-cellar.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-cellar.index');
    }
}

