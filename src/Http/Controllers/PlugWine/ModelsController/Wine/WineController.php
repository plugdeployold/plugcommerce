<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Wine;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Wine\WineRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Wine;

class WineController
{
    protected $response, $repository;

    public function __construct(WineRepository $repository)
    {
        $this->route_prefix = 'wine';

        $this->attribute = new Wine();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wines = $this->repository->findAll();
        
        return view('plugwine::wine.index', compact('wines'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wine = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine.admin.wine.edit', ['wine' => $wine->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wine = $this->repository->findOneById($id);
 
         return view('plugwine::wine.edit', compact('wine'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine.admin.wine.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine.admin.wine.index');
    }
}

