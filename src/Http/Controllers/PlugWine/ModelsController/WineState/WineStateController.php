<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineState;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineState\WineStateRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineState;

class WineStateController
{
    protected $response, $repository;

    public function __construct(WineStateRepository $repository)
    {
        $this->route_prefix = 'wine-state';

        $this->wineState = new WineState();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineStates  = $this->repository->findAll();
        
        return view('plugwine::wine-state.index', compact('wineStates'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-state.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineState = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-state.edit', ['wineState' => $wineState->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineState = $this->repository->findOneById($id);
 
         return view('plugwine::wine-state.edit', compact('wineState'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-state.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-state.index');
    }
}

