<?php

namespace WineCellar\StockSystem\Http\Controllers\Admin\Country;

use WineCellar\StockSystem\Repositories\Country\CountryRepository;

use WineCellar\StockSystem\Http\Controllers\Admin\ResourceController;

class CountryController extends ResourceController
{
    protected $response, $repository;

    public function __construct(CountryRepository $repository)
    {
        $this->route_prefix = 'country';

        // All Repository Functions:
        $this->repository = $repository;
    }

    public function index()
    {
        $data = $this->repository->index();

        $listingInfo = [
            'RoutePrefix' => $this->route_prefix,
            'PageTitle' => "Country Listing",
        ];

        return view('stocksystem::' . $this->route_prefix . '.index', compact('data', 'listingInfo'));
    }

    // API Functions V0.1
    public function findAll(){
        return $this->repository->findAll();
    }
}

