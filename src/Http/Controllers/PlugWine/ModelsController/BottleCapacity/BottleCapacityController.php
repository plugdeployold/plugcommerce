<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\BottleCapacity;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\BottleCapacity\BottleCapacityRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\BottleCapacity;

class BottleCapacityController
{
    protected $response, $repository;

    public function __construct(BottleCapacityRepository $repository)
    {
        $this->route_prefix = 'bottle-capacity';

        $this->wineGrape = new BottleCapacity();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $bottleCapacities  = $this->repository->findAll();
        
        return view('plugwine::bottle-capacity.index', compact('bottleCapacities'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::bottle-capacity.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $bottleCapacity = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::bottle-capacity.edit', ['bottleCapacity' => $bottleCapacity->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $bottleCapacity = $this->repository->findOneById($id);
 
         return view('plugwine::bottle-capacity.edit', compact('bottleCapacity'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::bottle-capacity.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::bottle-capacity.index');
    }
}

