<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\Activity;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\Activity\ActivityRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\Activity;

class ActivityController
{
    protected $response, $repository;

    public function __construct(ActivityRepository $repository)
    {
        $this->route_prefix = 'activity';

        $this->activity = new Activity();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $activities = $this->repository->findAll();
        
        return view('plugwine::activity.index', compact('activities'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::activity.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $activity = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::activity.edit', ['activity' => $activity->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $activity = $this->repository->findOneById($id);
 
         return view('plugwine::activity.edit', compact('activity'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::activity.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::activity.index');
    }
}

