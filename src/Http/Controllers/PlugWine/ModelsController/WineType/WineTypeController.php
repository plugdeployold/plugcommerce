<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\PlugWine\ModelsController\WineType;

use PlugDeploy\PlugCommerce\Repositories\PlugWine\WineType\WineTypeRepository;

use PlugDeploy\PlugCommerce\Models\PlugWine\WineType;

class WineTypeController
{
    protected $response, $repository;

    public function __construct(WineTypeRepository $repository)
    {
        $this->route_prefix = 'wine-type';

        $this->wineType = new WineType();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Wine View - 08-04-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $wineTypes = $this->repository->findAll();
        
        return view('plugwine::wine-type.index', compact('wineTypes'));
    }

     // ✅ Create Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugwine::wine-type.create');
     }
 
     // ✅ Store Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $wineType = $this->repository->store(request()->all());
 
         return redirect()->route('plugwine::wine-type.edit', ['wineType' => $wineType->id]);
     }
 
     // ✅ Edit Wine View - 08-04-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $wineType = $this->repository->findOneById($id);
 
         return view('plugwine::wine-type.edit', compact('wineType'));
     }
 
     // ✅ Update Wine - 08-04-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugwine::wine-type.index');
     }

     // ✅ Delete Wine - 08-04-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugwine::wine-type.index');
    }
}

