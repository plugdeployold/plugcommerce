<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\CMS;

use PlugDeploy\PlugCommerce\Repositories\CMS\Page\PageRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\CMS\Page;

class PageController
{
    protected $response, $repository;

    public function __construct(PageRepository $repository)
    {
        $this->route_prefix = 'page';

        $this->page = new Page();

        // All Repository Functions:
        $this->repository = $repository;
    }

    public function index()
    {
        $pages = $this->repository->findAll();
        
        return view('plugcommerce::products-menu.product.index', compact('pages'));
    }

    public function create()
    {
        return view('plugcommerce::products-menu.product.create');
    }

    public function store()
    {
        $product = $this->repository->store(request()->all());

        return redirect()->route('plugcommerce.admin.products.index');
        //return redirect()->route('plugcommerce.admin.products.edit', ['product' => $product->id]);
    }

    public function edit($id)
    {
        $product = $this->repository->findOneById($id);

        return view('plugcommerce::products-menu.product.edit', compact('product'));
    }

    public function update($id)
    {
        $this->repository->updateOneById($id, request()->all());

        return redirect()->route('plugcommerce.admin.products.index');
    }

    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugcommerce.admin.products.index');
    }

    public function findAll(){
        return $this->repository->findAll();
    }

    public function publish(){
        $this->repository->updateOneById(request()->page_id, request()->all());
        return response(request()->all(), 200);
    }
}