<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product;

use PlugDeploy\PlugCommerce\Repositories\Product\ProductRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Product;

class ProductController
{
    protected $response, $repository;

    // ✅ Construct Product With All Configs - 28-03-2021 - Jean-Pierre Carvalho 
    public function __construct(ProductRepository $repository)
    {
        $this->route_prefix = 'product';

        $this->product = new Product();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Product View - 28-03-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $products = $this->repository->findAll();
        
        return view('plugcommerce::products-menu.product.index', compact('products'));
    }

    // ✅ Create Product View - 28-03-2021 - Jean-Pierre Carvalho
    public function create()
    {
        return view('plugcommerce::products-menu.product.create');
    }

    // ✅ Store Product - 28-03-2021 - Jean-Pierre Carvalho
    public function store()
    {
        $product = $this->repository->store(request()->all());

        return redirect()->route('plugcommerce.admin.products.index');
        //return redirect()->route('plugcommerce.admin.products.edit', ['product' => $product->id]);
    }

    // ✅ Edit Product View - 28-03-2021 - Jean-Pierre Carvalho
    public function edit($id)
    {
        $product = $this->repository->findOneById($id);

        return view('plugcommerce::products-menu.product.edit', compact('product'));
    }

    // ✅ Update Product - 28-03-2021 - Jean-Pierre Carvalho
    public function update($id)
    {
        $this->repository->updateOneById($id, request()->all());

        return redirect()->route('plugcommerce.admin.products.index');
    }

    // ✅ Delete Product - 29-03-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugcommerce.admin.products.index');
    }

     // API Functions V0.1
     public function findAll(){
        return $this->repository->findAll();
    }
}