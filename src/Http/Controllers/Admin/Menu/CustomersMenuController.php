<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu;

class CustomersMenuController
{
    public function __construct()
    {
        
    }

    public function customers_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function groups_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function reviews_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function import()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function export()
    {
        return view('plugcommerce::products-menu.import');
    }
            
}

