<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu;

class OrdersMenuController
{
    public function __construct()
    {
        
    }

    public function orders_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function shipping_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function invoices_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function refunds_manager()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function automatic_invoices()
    {
        return view('plugcommerce::products-menu.import');
    }
            
}

