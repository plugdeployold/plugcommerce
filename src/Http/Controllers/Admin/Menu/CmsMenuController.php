<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu;

class CmsMenuController
{
    public function __construct()
    {
        
    }

    public function pages_manager()
    {
        $pages = \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\CMS\PageController@findAll');
        return view('plugcommerce::cms-menu.pages', compact('pages'));
    }

    public function translations_manager()
    {
        return view('plugcommerce::cms-menu.translations');
    }
            
}
