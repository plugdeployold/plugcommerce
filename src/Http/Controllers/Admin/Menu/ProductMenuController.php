<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product\ProductController as ProductController;
use PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category\CategoryController as CategoryController;

class ProductMenuController
{
    public function __construct()
    {
        
    }

    // ✅ Product Index With All Products
    public function products_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Product\ProductController@index');
    }

    public function categories_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category\CategoryController@index');
    }

    public function brands_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Brand\BrandController@index');
    }

    public function attributes_manager()
    {
        return \App::call('PlugDeploy\PlugCommerce\Http\Controllers\Admin\Attribute\AttributeController@index');
    }

    public function import()
    {
        return view('plugcommerce::products-menu.import');
    }

    public function export()
    {
        return view('plugcommerce::products-menu.export');
    }
            
}

