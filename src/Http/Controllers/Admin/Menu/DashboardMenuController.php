<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Menu;

class DashboardMenuController
{
    public function __construct()
    {
        
    }

    // ✅ Dashboard Index
    public function index()
    {
        return view('plugcommerce::dashboard.index');
    }
            
}

