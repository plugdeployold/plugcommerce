<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Attribute;

use PlugDeploy\PlugCommerce\Repositories\Attribute\AttributeRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Attribute;

class AttributeController
{
    protected $response, $repository;

    public function __construct(AttributeRepository $repository)
    {
        $this->route_prefix = 'attribute';

        $this->attribute = new Attribute();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Attribute View - 29-03-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $attributes = $this->repository->findAll();
        
        return view('plugcommerce::products-menu.attribute.index', compact('attributes'));
    }

     // ✅ Create Attribute View - 29-03-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugcommerce::products-menu.attribute.create');
     }
 
     // ✅ Store Attribute - 29-03-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $attribute = $this->repository->store(request()->all());
 
         return redirect()->route('plugcommerce.admin.attributes.edit', ['attribute' => $attribute->id]);
     }
 
     // ✅ Edit Attribute View - 29-03-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $attribute = $this->repository->findOneById($id);
 
         return view('plugcommerce::products-menu.attribute.edit', compact('attribute'));
     }
 
     // ✅ Update Attribute - 29-03-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugcommerce.admin.attributes.index');
     }

     // ✅ Delete Attribute - 29-03-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugcommerce.admin.attributes.index');
    }
}

