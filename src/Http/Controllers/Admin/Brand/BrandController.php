<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Brand;

use PlugDeploy\PlugCommerce\Repositories\Brand\BrandRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Brand;

class BrandController
{
    protected $response, $repository;

    public function __construct(BrandRepository $repository)
    {
        $this->route_prefix = 'brand';

        $this->brand = new Brand();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Brand View - 29-03-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $brands = $this->repository->findAll();
        
        return view('plugcommerce::products-menu.brand.index', compact('brands'));
    }

     // ✅ Create Brand View - 29-03-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugcommerce::products-menu.brand.create');
     }
 
     // ✅ Store Brand - 29-03-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $brand = $this->repository->store(request()->all());
 
         return redirect()->route('plugcommerce.admin.brands.edit', ['brand' => $brand->id]);
     }
 
     // ✅ Edit Brand View - 29-03-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $brand = $this->repository->findOneById($id);
 
         return view('plugcommerce::products-menu.brand.edit', compact('brand'));
     }
 
     // ✅ Update Brand - 29-03-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugcommerce.admin.brands.index');
     }

    // ✅ Delete Brand - 29-03-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugcommerce.admin.brands.index');
    }
}

