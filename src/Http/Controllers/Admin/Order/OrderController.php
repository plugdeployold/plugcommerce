<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Order;

use PlugDeploy\PlugCommerce\Repositories\Order\OrderRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Order;

class OrderController
{
    protected $response, $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->route_prefix = 'order';

        $this->order = new Order();

        // All Repository Functions:
        $this->repository = $repository;
    }

    public function index()
    {
        $data = $this->repository->index();

        return $data;

        //return view('stocksystem::' . $this->route_prefix . '.index', compact('data'));
    }

    public function show($id)
    {
        $data = $this->repository->show($id);

        return $data;
    }

    // API Functions V0.1
    public function findAll(){
        return $this->repository->findAll();
    }
}

