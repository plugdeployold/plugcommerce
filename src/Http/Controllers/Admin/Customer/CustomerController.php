<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Customer;

use PlugDeploy\PlugCommerce\Repositories\Customer\CustomerRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Customer;

class CustomerController
{
    protected $response, $repository;

    public function __construct(CustomerRepository $repository)
    {
        $this->route_prefix = 'customer';

        $this->customer = new Customer();

        // All Repository Functions:
        $this->repository = $repository;
    }

    public function index()
    {
        $data = $this->repository->index();

        return $data;

        //return view('stocksystem::' . $this->route_prefix . '.index', compact('data'));
    }

    public function show($id)
    {
        $data = $this->repository->show($id);

        return $data;
    }

    // API Functions V0.1
    public function findAll(){
        return $this->repository->findAll();
    }
}

