<?php

namespace PlugDeploy\PlugCommerce\Http\Controllers\Admin\Category;

use PlugDeploy\PlugCommerce\Repositories\Category\CategoryRepository;

use PlugDeploy\PlugCommerce\Http\Controllers\Admin\ResourceController;

use PlugDeploy\PlugCommerce\Models\Category;

class CategoryController
{
    protected $response, $repository;

    // ✅ Construct Category With All Configs - 29-03-2021 - Jean-Pierre Carvalho 
    public function __construct(CategoryRepository $repository)
    {
        $this->route_prefix = 'category';

        $this->category = new Category();

        // All Repository Functions:
        $this->repository = $repository;
    }

    // ✅ See All Category View - 28-03-2021 - Jean-Pierre Carvalho 
    public function index()
    {
        $categories = $this->repository->findAll();
        
        return view('plugcommerce::products-menu.category.index', compact('categories'));
    }

     // ✅ Create Category View - 29-03-2021 - Jean-Pierre Carvalho
     public function create()
     {
         return view('plugcommerce::products-menu.category.create');
     }
 
     // ✅ Store Category - 29-03-2021 - Jean-Pierre Carvalho
     public function store()
     {
         $category = $this->repository->store(request()->all());
 
         return redirect()->route('plugcommerce.admin.categories.edit', ['category' => $category->id]);
     }
 
     // ✅ Edit Category View - 29-03-2021 - Jean-Pierre Carvalho
     public function edit($id)
     {
         $category = $this->repository->findOneById($id);
 
         return view('plugcommerce::products-menu.category.edit', compact('category'));
     }
 
     // ✅ Update Category - 29-03-2021 - Jean-Pierre Carvalho
     public function update($id)
     {
         $this->repository->updateOneById($id, request()->all());
 
         return redirect()->route('plugcommerce.admin.categories.index');
     }

    // ✅ Delete Category - 29-03-2021 - Jean-Pierre Carvalho
    public function destroy($id)
    {
        $this->repository->deleteOneById($id);

        return redirect()->route('plugcommerce.admin.categories.index');
    }
}

