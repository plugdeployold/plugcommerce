<?php

namespace PlugDeploy\PlugCommerce\Main;

class SideMenu
{
    /**
     * List of side menu items.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public static function menu()
    {

        $plugwine = [
            'icon' => 'home',
            'title' => 'Gestão de Vinhos',
            'sub_menu' => [
                'wines_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wines_manager',
                     'params' => [],
                    'title' => 'Vinhos'
                ],
                'bottles_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.bottles_manager',
                     'params' => [],
                    'title' => 'Garrafas'
                ],
                'wine_types_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_types_manager',
                     'params' => [],
                    'title' => 'Tipos de Vinhos'
                ],
                'wine_grapes_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_grapes_manager',
                     'params' => [],
                    'title' => 'Castas'
                ],
                'bottle_capacities_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.bottle_capacities_manager',
                     'params' => [],
                    'title' => 'Capacidade das Garrafas'
                ],
                'wine_photos_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_photos_manager',
                     'params' => [],
                    'title' => 'Fotos dos Vinhos'
                ],
                'regions_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.regions_manager',
                     'params' => [],
                    'title' => 'Regiões'
                ],
                'wine_makers_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_makers_manager',
                     'params' => [],
                    'title' => 'Enólogos'
                ],
                'producers_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.producers_manager',
                     'params' => [],
                    'title' => 'Produtores'
                ],
                'wine_farms_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_farms_manager',
                     'params' => [],
                    'title' => 'Quintas'
                ],
                'wine_states_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_states_manager',
                     'params' => [],
                    'title' => 'Estados de Vinho'
                ],
                'wine_cellars_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_cellars_manager',
                     'params' => [],
                    'title' => 'Garrafeiras'
                ],
                'wine_cellar_sections_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_cellar_sections_manager',
                     'params' => [],
                    'title' => 'Secções da Garrafeira'
                ],
                'wine_tasting_notes_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.wine_tasting_notes_manager',
                     'params' => [],
                    'title' => 'Notas de Prova'
                ],
                'activities_manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.plugwine.activities_manager',
                     'params' => [],
                    'title' => 'Actividades'
                ],
            ]
        ];

        $plugcommerce_products = [
            'icon' => 'home',
            'title' => 'Produtos',
            'sub_menu' => [
                'products-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.products.manager',
                     'params' => [],
                    'title' => 'Gestão de Produtos'
                ],
                'categories-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.categories.manager',
                     'params' => [],
                    'title' => 'Gestão de Categorias'
                ],
                'brands-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.brands.manager',
                     'params' => [],
                    'title' => 'Gestão de Marcas'
                ],
                'attributes-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.attributes.manager',
                     'params' => [],
                    'title' => 'Gestão de Atributos'
                ],
                'import' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.products.import',
                     'params' => [],
                    'title' => 'Importação'
                ],
                'export' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.products.export',
                     'params' => [],
                    'title' => 'Exportação'
                ],
            ]
            ];
        
        $plugcommerce_orders = [
            'icon' => 'home',
            'title' => 'Encomendas',
            'sub_menu' => [
                'orders-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.orders.orders_manager',
                     'params' => [],
                    'title' => 'Gerir Encomendas'
                ],
                'shipping-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.orders.shipping_manager',
                     'params' => [],
                    'title' => 'Gerir Envios'
                ],
                'invoices-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.orders.invoices_manager',
                     'params' => [],
                    'title' => 'Gerir Faturas'
                ],
                'refunds-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.orders.refunds_manager',
                     'params' => [],
                    'title' => 'Gerir Devoluções'
                ],
                'automatic-invoice' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.orders.automatic_invoices',
                     'params' => [],
                    'title' => 'Faturação Automática'
                ],
            ]
            ];

        $plugcommerce_customers = [
            'icon' => 'home',
            'title' => 'Clientes',
            'sub_menu' => [
                'customer-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.customers.customers_manager',
                     'params' => [],
                    'title' => 'Gestão de Clientes'
                ],
                'groups-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.customers.groups_manager',
                     'params' => [],
                    'title' => 'Grupos'
                ],
                'reviews-manager' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.customers.reviews_manager',
                     'params' => [],
                    'title' => 'Reviews'
                ],
                'import' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.customers.import',
                     'params' => [],
                    'title' => 'Importação'
                ],
                'export' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.customers.export',
                     'params' => [],
                    'title' => 'Exportação'
                ],
            ]
            ];

        $plugcommerce_cms = [
            'icon' => 'home',
            'title' => 'CMS',
            'sub_menu' => [
                'pages' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.cms-menu.pages',
                     'params' => [],
                    'title' => 'Páginas'
                ],
                'translations' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.cms-menu.translations',
                     'params' => [],
                    'title' => 'Traduções'
                ],
            ]
            ];

        $plugcommerce_layout = [
            'icon' => 'home',
            'title' => 'Layout',
            'sub_menu' => [
                'settings' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.settings',
                     'params' => [],
                    'title' => 'Configurações'
                ],
                'theme' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.theme',
                     'params' => [],
                    'title' => 'Tema'
                ],
                'banners' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.banners',
                     'params' => [],
                    'title' => 'Banners'
                ],
                'emails' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.emails',
                     'params' => [],
                    'title' => 'Emails'
                ],
                'popups' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.popups',
                     'params' => [],
                    'title' => 'Popups'
                ],
                'forms' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.layout.forms',
                     'params' => [],
                    'title' => 'Formulários'
                ],
            ]
            ];
        
        $plugcommerce_marketing = [
            'icon' => 'home',
            'title' => 'Marketing',
            'sub_menu' => [
                'cupons' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.cupons',
                     'params' => [],
                    'title' => 'Vales de Desconto'
                ],
                'pixels' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.pixels',
                     'params' => [],
                    'title' => 'Pixeis de Conversão'
                ],
                'social' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.social',
                     'params' => [],
                    'title' => 'Redes Sociais'
                ],
                'channels' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.channels',
                     'params' => [],
                    'title' => 'Canais de Venda'
                ],
                'newsletters' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.newsletters',
                     'params' => [],
                    'title' => 'Neswletters'
                ],
                'seo' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.marketing.seo',
                     'params' => [],
                    'title' => 'SEO'
                ],
            ]
            ];

        $plugcommerce_settings = [
            'icon' => 'home',
            'title' => 'Configurações',
            'sub_menu' => [
                'payments' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.payments',
                     'params' => [],
                    'title' => 'Meios de Pagamento'
                ],
                'languages' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.languages',
                     'params' => [],
                    'title' => 'Idiomas'
                ],
                'currencies' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.currencies',
                     'params' => [],
                    'title' => 'Moedas'
                ],
                'domains' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.domains',
                     'params' => [],
                    'title' => 'Dominios'
                ],
                'carriers' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.carriers',
                     'params' => [],
                    'title' => 'Portes e Entregas'
                ],
                'taxes' => [
                    'icon' => 'home',
                    'route_name' => 'plugcommerce.admin.settings.taxes',
                     'params' => [],
                    'title' => 'Taxas e Impostos'
                ],
            ]
            ];
        
        $plugcommerce_appcenter = [
            'icon' => 'home',
                    /*'route_name' => '',*/
                     'params' => [],
                    'title' => 'App Center'
                ];
            
        $plugcommerce_api = [
            'icon' => 'home',
                    /*'route_name' => '',*/
                     'params' => [],
                    'title' => 'API'
                ];

        $plugcommerce_help = [
            'icon' => 'home',
                    /*'route_name' => '',*/
                     'params' => [],
                    'title' => 'Ajuda'
                ];

        return [
            'dashboard' => [
                'icon' => 'home',
                'title' => 'Painel de Controlo',
                'route_name' => 'plugcommerce.admin.dashboard',
                'params' => [],
                /*'sub_menu' => [
                    'products-manager' => [
                        'icon' => 'home',
                        'route_name' => 'stocksystem.admin.activities.index',
                        'params' => [
                            'layout' => 'side-menu',
                        ],
                        'title' => 'Gestão de Produtos'
                    ],
                ]*/
            ],
            /*'dashboard2' => [
                'icon' => 'home',
                'title' => 'Vatsal',
                'route_name' => 'plugcommerce.admin.dashboard',
                'params' => [
                    'layout' => 'side-menu',
                ],
            ],*/
            'devider',
            'plugwine' => env('PLUGWINE_MENU_STATUS', false) ? $plugwine : null,
            'plugcommerce_products' => env('PLUGWCOMMERCE_PRODUCTS_MENU_STATUS', false) ? $plugcommerce_products : null,
            'plugcommerce_orders' => env('PLUGWCOMMERCE_ORDERS_MENU_STATUS', false) ? $plugcommerce_orders : null,
            'plugcommerce_customers' => env('PLUGWCOMMERCE_CUSTOMERS_MENU_STATUS', false) ? $plugcommerce_customers : null,
            'plugcommerce_cms' => env('PLUGWCOMMERCE_CMS_MENU_STATUS', false) ? $plugcommerce_cms : null,
            'plugcommerce_layout' => env('PLUGWCOMMERCE_LAYOUT_MENU_STATUS', false) ? $plugcommerce_layout : null,
            'plugcommerce_marketing' => env('PLUGWCOMMERCE_MARKETIG_MENU_STATUS', false) ? $plugcommerce_marketing : null,
            'plugcommerce_settings' => env('PLUGWCOMMERCE_SETTINGS_MENU_STATUS', false) ? $plugcommerce_settings : null,
            'devider',
            'plugcommerce_appcenter' => env('PLUGWCOMMERCE_APPCENTER_MENU_STATUS', false) ? $plugcommerce_appcenter : null,
            'plugcommerce_api' => env('PLUGWCOMMERCE_API_MENU_STATUS', false) ? $plugcommerce_api : null,
            'devider',
            'plugcommerce_help' => env('PLUGWCOMMERCE_HELP_MENU_STATUS', false) ? $plugcommerce_help : null,
        ];
    }
}
