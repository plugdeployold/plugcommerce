<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('attributes');

        // Verifica se a tabela existe
        if(Schema::hasTable('attributes'))
        {
            // Se sim, adiciona os novos campos à tabela
            Schema::table('attributes', function (Blueprint $table) {
               //$table->string('brand');
            });
        }
        else {
            Schema::create('attributes', function (Blueprint $table) {
                $table->id();
                $table->string('name')->default('')->unique();
                $table->string('description')->default('');
                $table->string('category')->default('Categoria 2');
                $table->string('imageSrc')->default('https://i.pinimg.com/originals/8c/d5/97/8cd5976ac79e47b5c93f237197240b40.jpg');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
