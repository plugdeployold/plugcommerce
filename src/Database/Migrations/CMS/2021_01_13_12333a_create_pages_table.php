<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/*

Documentation:

Na tabela page__pages, é guardado 

*/

/*

INSERT INTO `plugwine`.`cms__pages` (`id`, `name`, `type`, `is_home`) VALUES ('2', 'V_Guard_Clip', 'Página Institucional', '0');
INSERT INTO `plugwine`.`cms__pages` (`name`, `type`, `is_home`) VALUES ('V_Guard_Patch', 'Página Institucional', '0');
INSERT INTO `plugwine`.`cms__pages` (`name`, `type`, `is_home`) VALUES ('V_Guard_Gate', 'Página Institucional', '0');
INSERT INTO `plugwine`.`cms__pages` (`name`, `type`, `is_home`) VALUES ('Sobre Nós', 'Página Institucional', '0');

*/

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*Schema::dropIfExists('cms__pages_translations', function (Blueprint $table){
            $table->dropForeign(['page_id']);
            $table->dropColumn('page_id');
        });*/

        //Schema::dropIfExists('cms__pages');

        // Verifica se a tabela existe
        if(Schema::hasTable('cms__pages'))
        {
            Schema::table('cms__pages', function (Blueprint $table) {

            });
        }
        else {
            Schema::create('cms__pages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('type');
                $table->boolean('is_home')->default(0);
                $table->string('slug');
                $table->string('controller_name');
                $table->string('route_name');
                $table->boolean('published')->default(0);
                $table->boolean('status')->default(1);
                $table->timestamps();
            });
        }

        // Verifica se a tabela existe
        if(Schema::hasTable('cms__page_translations'))
        {
            Schema::table('page__page_translations', function (Blueprint $table) {

            });
        }
        else {
            Schema::create('cms__page_translations', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('page_id')->unsigned();
                $table->string('locale')->index();
    
                $table->string('title');
                $table->string('slug');
                $table->boolean('status')->default(1);
                $table->text('body');
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('og_title')->nullable();
                $table->string('og_description')->nullable();
                $table->string('og_image')->nullable();
                $table->string('og_type')->nullable();
    
                $table->unique(['page_id', 'locale']);
                $table->foreign('page_id')->references('id')->on('cms__pages')->onDelete('cascade');
                $table->timestamps();
            });
        } 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('page__pages');
        Schema::dropIfExists('page__page_translations');
    }
}
