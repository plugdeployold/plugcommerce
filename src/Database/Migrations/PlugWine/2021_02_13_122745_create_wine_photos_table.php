<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinePhotosTable extends Migration
{
      /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('wine_photos');

        // Verifica se a tabela existe
        if(Schema::hasTable('wine_photos'))
        {
            // Se sim, adiciona os novos campos à tabela
            Schema::table('wine_photos', function (Blueprint $table) {
               //$table->string('brand');
            });
        }
        else {
            // Se não, cria a tabela
            Schema::create('wine_photos', function (Blueprint $table) {
                $table->id();
                $table->string('name')->unique();
                $table->string('url')->unique();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

