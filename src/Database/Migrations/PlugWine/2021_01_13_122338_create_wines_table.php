<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wines', function (Blueprint $table) {
            $table->id();
            // ID do país de fabrico
            $table->integer('country_id')->nullable();
            // ID do produtor
            $table->integer('producer_id')->nullable();
            // ID da quinta
            $table->integer('quinta_id')->nullable();
            // ID da região
            $table->integer('region_id')->nullable();
            // ID do tipo de vinho
            $table->integer('wine_type_id')->nullable();
            // ID ???
            $table->integer('wine_variety_id')->nullable();
            // ID do Enólogo
            $table->integer('winemaker_id')->nullable();
            // Nome
            $table->string('name');
            // Descrição
            $table->longText('description')->nullable();
            // Temperatura mínima para se beber
            $table->float('temp_min', '16', '2')->nullable();
            // Temperatura máxima para se beber
            $table->float('temp_max', '16', '2')->nullable();
            // Percentagem de álcool
            $table->float('alcohol_percentage', '16', '2')->nullable();
            // Ano de colheita
            $table->date('harvest_year')->nullable();
            // Ano de engarrafamento
            $table->date('bottling_year')->nullable();
            // Tempo de estágio
            $table->dateTime('time_wine_stage')->nullable();
            // Estado do vinho
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wines');
    }
}
