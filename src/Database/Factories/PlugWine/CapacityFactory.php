<?php

namespace WineCellar\StockSystem\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WineCellar\StockSystem\Models\Capacity;

class CapacityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Capacity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  =>  $this->faker->name
        ];
    }
}
