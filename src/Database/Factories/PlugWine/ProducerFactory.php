<?php

namespace WineCellar\StockSystem\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WineCellar\StockSystem\Models\Producer;

class ProducerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Producer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  =>  $this->faker->name
        ];
    }
}
