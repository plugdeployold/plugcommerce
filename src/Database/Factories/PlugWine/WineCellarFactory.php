<?php

namespace WineCellar\StockSystem\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WineCellar\StockSystem\Models\WineCellar;

class WineCellarFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WineCellar::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  =>  $this->faker->name
        ];
    }
}
