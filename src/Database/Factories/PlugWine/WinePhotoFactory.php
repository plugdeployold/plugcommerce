<?php

namespace WineCellar\StockSystem\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WineCellar\StockSystem\Models\WinePhoto;

class WinePhotoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WinePhoto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'  =>  $this->faker->name
        ];
    }
}
