<?php

namespace WineCellar\StockSystem\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use WineCellar\StockSystem\Models\Wine;

class WineFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Wine::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
                'name'              =>  $this->faker->name,
                'country_id'        =>  $this->faker->numberBetween($min = 1, $max = 100),
                'producer_id'       =>  $this->faker->numberBetween($min = 1, $max = 100),
                'quinta_id'         =>  $this->faker->numberBetween($min = 1, $max = 100),
                'region_id'         =>  $this->faker->numberBetween($min = 1, $max = 100),
                'wine_type_id'      =>  $this->faker->numberBetween($min = 1, $max = 100),
                'wine_variety_id'   =>  $this->faker->numberBetween($min = 1, $max = 100),
                'winemaker_id'      =>  $this->faker->numberBetween($min = 1, $max = 100),
                'description'       =>  $this->faker->numberBetween($min = 1, $max = 100),
                'temp_min'          =>  $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 5),
                'temp_max'          =>  $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = 5)   ,
                'alcohol_percentage'=>  $this->faker->randomFloat($nbMaxDecimals = NULL, $min = 20, $max = 50),
                'harvest_year'      =>  $this->faker->date($format = 'Y-m-d', $max = 'now'),
                'bottling_year'     =>  $this->faker->date($format = 'Y-m-d', $max = 'now'),
                'time_wine_stage'   =>  $this->faker->date($format = 'Y-m-d H:s', $max = 'now'),
                'status'            =>  $this->faker->numberBetween($min = 0, $max = 1)
            ];
    }
}
