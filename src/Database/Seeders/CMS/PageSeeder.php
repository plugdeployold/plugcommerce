<?php

namespace PlugDeploy\PlugCommerce\Database\Seeders\CMS;

use Illuminate\Database\Seeder;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \PlugDeploy\PlugCommerce\Models\CMS\Page::insert([
            [
                'name' => 'Homepage',
                'type' => 'Página Institucional',
                'is_home' => 1,
                'slug' => '',
                'controller_name' => 'index',
                'route_name' => 'theme.vplusguard.pages.index',
                'published' => 1,
                'status' => 1,
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\CMS\Page::insert([
            [
                'name' => 'Sobre Nós',
                'type' => 'Página Institucional',
                'is_home' => 0,
                'slug' => 'about-us',
                'controller_name' => 'about',
                'route_name' => 'theme.vplusguard.pages.about',
                'published' => 1,
                'status' => 1,
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\CMS\Page::insert([
            [
                'name' => 'Produto Clip',
                'type' => 'Página Institucional',
                'is_home' => 0,
                'slug' => 'vguard-clip',
                'controller_name' => 'vguard_clip',
                'route_name' => 'theme.vplusguard.pages.vguard_clip',
                'published' => 1,
                'status' => 1,
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\CMS\Page::insert([
            [
                'name' => 'Produto Patch',
                'type' => 'Página Institucional',
                'is_home' => 0,
                'slug' => 'vguard-patch',
                'controller_name' => 'vguard_patch',
                'route_name' => 'theme.vplusguard.pages.vguard_patch',
                'published' => 1,
                'status' => 1,
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\CMS\Page::insert([
            [
                'name' => 'Produto Guard',
                'type' => 'Página Institucional',
                'is_home' => 0,
                'slug' => 'vguard-gate',
                'controller_name' => 'vguard_gate',
                'route_name' => 'theme.vplusguard.pages.vguard_gate',
                'published' => 1,
                'status' => 1,
            ],
        ]);
    }
}