<?php

namespace WineCellar\StockSystem\Database\Seeders;

use Illuminate\Database\Seeder;
use WineCellar\StockSystem\Models\WinePhoto;

class WinePhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        \WineCellar\StockSystem\Models\WinePhoto::factory(5)->create();
    }
}
