<?php

namespace WineCellar\StockSystem\Database\Seeders;

use Illuminate\Database\Seeder;
use WineCellar\StockSystem\Models\Country;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        \WineCellar\StockSystem\Models\Country::factory(5)->create();
    }
}
