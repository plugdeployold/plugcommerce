<?php

namespace WineCellar\StockSystem\Database\Seeders;

use Illuminate\Database\Seeder;
use WineCellar\StockSystem\Models\WineCellar;

class WineCellarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        \WineCellar\StockSystem\Models\WineCellar::factory(5)->create();
    }
}
