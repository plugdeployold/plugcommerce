<?php

namespace PlugDeploy\PlugCommerce\Database\Seeders\PlugWine;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use PlugDeploy\PlugCommerce\Models\PlugWine\Wine;

class WineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        //\WineCellar\StockSystem\Models\Wine::factory(5)->create();

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Quinta Terra Gold Edition',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Buçaco 2002',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Buçaco 2015',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Quinta Maria Isabel Vinhas da Princesa',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Quinta do Quetzal Reserva da Familia 2012',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Anselmo Mendes Parcela Unica',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Quinta das Bageiras Pai Abel',
            ],
        ]);

        \PlugDeploy\PlugCommerce\Models\PlugWine\Wine::insert([
            [
                'name' => 'Pêra-Manca',
            ],
        ]);
    }
}



