<?php

namespace WineCellar\StockSystem\Database\Seeders;

use Illuminate\Database\Seeder;
use WineCellar\StockSystem\Models\WineGrape;

class WineGrapeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        \WineCellar\StockSystem\Models\WineGrape::factory(5)->create();
    }
}
