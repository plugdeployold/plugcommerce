<?php

namespace WineCellar\StockSystem\Database\Seeders;

use Illuminate\Database\Seeder;
use WineCellar\StockSystem\Models\BottleCapacity;

class BottleCapacitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Fake wines
        \WineCellar\StockSystem\Models\BottleCapacity::factory(5)->create();
    }
}
