<?php

namespace WineCellar\StockSystem\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class WineDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    protected $itemsPerPage = 10;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepareQueryBuilder()
    {
        /* query builder */
        $queryBuilder = DB::table('winecellar_wines')->select('id')->addSelect('name', 'description');

        $this->addFilter('name', 'name');
        $this->addFilter('description', 'description');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index' => 'name',
            'label' => trans('stocksystem::app.wines.model.name'),
            'type' => 'string',
            'searchable' => false,
            'sortable' => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index' => 'description',
            'label' => trans('stocksystem::app.wines.model.description'),
            'type' => 'string',
            'searchable' => true,
            'sortable' => true,
            'filterable' => true,
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title' => trans('stocksystem.admin.wines.edit'),
            'method' => 'GET',
            'route' => 'stocksystem.admin.wines.edit',
            'icon' => 'icon pencil-lg-icon',
            'condition' => function () {
                return true;
            },
        ]);

        /*$this->addAction([
    'title' => trans('stocksystem.admin.wines.delete'),
    'method' => 'POST',
    'route' => 'stocksystem.admin.wines.delete',
    'confirm_text' => trans('ui::app.datagrid.massaction.delete', ['resource' => 'product']),
    'icon' => 'icon trash-icon',
    ]);*/
    }

    public function prepareMassActions()
    {
        $this->addAction([
            'title' => trans('admin::app.datagrid.copy'),
            'method' => 'GET',
            'route' => 'admin.catalog.products.copy',
            'icon' => 'icon copy-icon',
        ]);

        $this->addMassAction([
            'type' => 'delete',
            'label' => trans('admin::app.datagrid.delete'),
            'action' => route('admin.catalog.products.massdelete'),
            'method' => 'DELETE',
        ]);

        $this->addMassAction([
            'type' => 'update',
            'label' => trans('admin::app.datagrid.update-status'),
            'action' => route('admin.catalog.products.massupdate'),
            'method' => 'PUT',
            'options' => [
                'Active' => 1,
                'Inactive' => 0,
            ],
        ]);
    }
}
