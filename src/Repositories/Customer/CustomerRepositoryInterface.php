<?php

namespace PlugDeploy\PlugCommerce\Repositories\Customer;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface CustomerRepositoryInterface extends RepositoryInterface
{

}
