<?php

namespace PlugDeploy\PlugCommerce\Repositories\PlugWine\WineCellarSection;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface WineCellarSectionRepositoryInterface extends RepositoryInterface
{

}
