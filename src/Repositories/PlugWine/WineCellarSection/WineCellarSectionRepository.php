<?php

namespace PlugDeploy\PlugCommerce\Repositories\PlugWine\WineCellarSection;

use PlugDeploy\PlugCommerce\Repositories\Eloquent\AbstractEloquentRepository;
use PlugDeploy\PlugCommerce\Models\PlugWine\WineCellarSection;

class WineCellarSectionRepository extends AbstractEloquentRepository implements WineCellarSectionRepositoryInterface
{
    protected $model;

    public function __construct(WineCellarSection $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}