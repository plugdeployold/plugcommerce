<?php

namespace PlugDeploy\PlugCommerce\Repositories\PlugWine\WineType;

use PlugDeploy\PlugCommerce\Repositories\Eloquent\AbstractEloquentRepository;
use PlugDeploy\PlugCommerce\Models\PlugWine\WineType;

class WineTypeRepository extends AbstractEloquentRepository implements WineTypeRepositoryInterface
{
    protected $model;

    public function __construct(WineType $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}