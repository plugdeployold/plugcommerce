<?php

namespace PlugDeploy\PlugCommerce\Repositories\PlugWine\WineFarm;

use PlugDeploy\PlugCommerce\Repositories\Eloquent\AbstractEloquentRepository;
use PlugDeploy\PlugCommerce\Models\PlugWine\WineFarm;

class WineFarmRepository extends AbstractEloquentRepository implements WineFarmRepositoryInterface
{
    protected $model;

    public function __construct(WineFarm $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}