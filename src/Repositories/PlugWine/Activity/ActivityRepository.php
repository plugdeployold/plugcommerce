<?php

namespace PlugDeploy\PlugCommerce\Repositories\PlugWine\Activity;

use PlugDeploy\PlugCommerce\Repositories\Eloquent\AbstractEloquentRepository;
use PlugDeploy\PlugCommerce\Models\PlugWine\Activity;

class ActivityRepository extends AbstractEloquentRepository implements ActivityRepositoryInterface
{
    protected $model;

    public function __construct(Activity $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}