<?php

namespace PlugDeploy\PlugCommerce\Repositories\Product;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface ProductRepositoryInterface extends RepositoryInterface
{

}
