<?php

namespace WineCellar\StockSystem\Repositories\WinePhoto;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WinePhotoRepositoryInterface extends RepositoryInterface
{

}
