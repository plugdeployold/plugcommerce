<?php

namespace WineCellar\StockSystem\Repositories\BottleCapacity;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface BottleCapacityRepositoryInterface extends RepositoryInterface
{

}
