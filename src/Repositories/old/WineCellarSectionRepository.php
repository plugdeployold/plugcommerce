<?php

namespace WineCellar\StockSystem\Repositories;

use WineCellar\StockSystem\Models\WineCellarSection;

class WineCellarSectionRepository
{

    protected $model;

    public function __construct(WineCellarSection $model)
    {
        $this->model = $model;
    }


    public function index()
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function show($id)
    {
        return $this->model->find($id);
    }

    public function update($id, $request)
    {
        $wine = $this->model->find($id);

        $wine->update($request);
    }

    public function delete($id)
    {
        $wine = $this->model->find($id);

        $wine->delete();

        return $wine;
    }

}
