<?php

/*
    Validado a 24-02-2021 por Jean-Pierre Carvalho
*/

namespace PlugDeploy\PlugCommerce\Repositories\Bottle;

use PlugDeploy\PlugCommerce\Repositories\Eloquent\AbstractEloquentRepository;
use PlugDeploy\PlugCommerce\Models\Bottle;

class BottleRepository extends AbstractEloquentRepository implements BottleRepositoryInterface
{
    protected $model;

    public function __construct(Bottle $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}
