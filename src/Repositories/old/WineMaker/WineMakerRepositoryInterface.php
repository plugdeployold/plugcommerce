<?php

namespace WineCellar\StockSystem\Repositories\WineMaker;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineMakerRepositoryInterface extends RepositoryInterface
{

}
