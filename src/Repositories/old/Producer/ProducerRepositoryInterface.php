<?php

namespace WineCellar\StockSystem\Repositories\Producer;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface ProducerRepositoryInterface extends RepositoryInterface
{

}
