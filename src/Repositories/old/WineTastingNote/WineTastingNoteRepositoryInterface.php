<?php

namespace WineCellar\StockSystem\Repositories\WineTastingNote;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineTastingNoteRepositoryInterface extends RepositoryInterface
{

}
