<?php

namespace WineCellar\StockSystem\Repositories\WineCellar;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineCellarRepositoryInterface extends RepositoryInterface
{

}
