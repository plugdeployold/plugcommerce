<?php

namespace WineCellar\StockSystem\Repositories\WineCellar;

use WineCellar\StockSystem\Repositories\Eloquent\AbstractEloquentRepository;
use WineCellar\StockSystem\Models\WineCellar;

class WineCellarRepository extends AbstractEloquentRepository implements WineCellarRepositoryInterface
{
    protected $model;

    public function __construct(WineCellar $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}
