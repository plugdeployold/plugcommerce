<?php

namespace WineCellar\StockSystem\Repositories\WineCellarSection;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineCellarSectionRepositoryInterface extends RepositoryInterface
{

}
