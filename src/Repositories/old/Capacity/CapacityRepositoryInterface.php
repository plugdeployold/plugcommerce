<?php

namespace WineCellar\StockSystem\Repositories\Capacity;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface CapacityRepositoryInterface extends RepositoryInterface
{

}
