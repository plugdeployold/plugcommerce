<?php

namespace WineCellar\StockSystem\Repositories\WineType;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineTypeRepositoryInterface extends RepositoryInterface
{

}
