<?php

namespace WineCellar\StockSystem\Repositories\WineGrape;

use WineCellar\StockSystem\Repositories\Eloquent\AbstractEloquentRepository;
use WineCellar\StockSystem\Models\WineGrape;

class WineGrapeRepository extends AbstractEloquentRepository implements WineGrapeRepositoryInterface
{
    protected $model;

    public function __construct(WineGrape $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return  $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }
}
