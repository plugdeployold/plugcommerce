<?php

namespace WineCellar\StockSystem\Repositories\Wine;

use WineCellar\StockSystem\Repositories\Repository\RepositoryInterface;

interface WineRepositoryInterface extends RepositoryInterface
{

}
