<?php

/*
    Validado a 24-02-2021 por Jean-Pierre Carvalho
*/

namespace WineCellar\StockSystem\Repositories\Wine;

use WineCellar\StockSystem\Repositories\Eloquent\AbstractEloquentRepository;
use WineCellar\StockSystem\Models\Wine;

class WineRepository extends AbstractEloquentRepository implements WineRepositoryInterface
{
    protected $model;

    public function __construct(Wine $model)
    {
        parent::__construct($model);
    }

    public function index()
    {
        return $this->findAll();
    }

    public function create(array $data)
    {
        return $this->create($data);
    }

    public function show($id)
    {
        return $this->findOneById($id);
    }

    public function update($id, $request)
    {
        return $this->updateOneById($id, $request);
    }

    public function delete($id)
    {
        return $this->deleteOneById($id);
    }

}
