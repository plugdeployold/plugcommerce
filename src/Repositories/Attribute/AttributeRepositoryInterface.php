<?php

namespace PlugDeploy\PlugCommerce\Repositories\Attribute;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface AttributeRepositoryInterface extends RepositoryInterface
{

}
