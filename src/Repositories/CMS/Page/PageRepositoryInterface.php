<?php

namespace PlugDeploy\PlugCommerce\Repositories\CMS\Page;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface PageRepositoryInterface extends RepositoryInterface
{

}
