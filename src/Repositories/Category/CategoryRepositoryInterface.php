<?php

namespace PlugDeploy\PlugCommerce\Repositories\Category;

use PlugDeploy\PlugCommerce\Repositories\Repository\RepositoryInterface;

interface CategoryRepositoryInterface extends RepositoryInterface
{

}
