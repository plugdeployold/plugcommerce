<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

// composer install
// ./vendor/phpunit/phpunit/phpunit tests --debug

final class WineTest extends TestCase
{
    private $localhost = "localhost";
    private $port = "9001";

    // Test Get Method
    public function testIndexMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)->returnResponseObject()->get();

        $this->assertEquals($response->status, 200);
    }

    public function testCreateMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/create";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)->returnResponseObject()->get();

        $this->assertEquals($response->status, 200);
    }

    public function testStoreMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/create";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)
            ->withData([])
            ->returnResponseObject()
            ->post();

        $this->assertEquals($response->status, 201);
    }

    public function testShowMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/show";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)->returnResponseObject()->get();

        $this->assertEquals($response->status, 200);
    }

    public function testEditMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/edit/1";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)
            ->returnResponseObject()
            ->get();

        $this->assertEquals($response->status, 200);
    }

    public function testUpdateMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/update/1";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)
            ->withData([])
            ->returnResponseObject()
            ->put();

        $this->assertEquals($response->status, 200);
    }

    public function testDestroyMethod(): void
    {
        $url = "http://$this->localhost:$this->port/admin/stocksystem/wines/delete/1";

        $response = (new PlugDeploy\Core\Curl\Builder())->to($url)->returnResponseObject()
            ->delete();

        $this->assertEquals($response->status, 200);
    }

}


/*
 *         - step:
            name: Lint
            script:
              - composer install
              - ./vendor/bin/phplint . --exclude=vendor
            caches:
              - composer
 */

