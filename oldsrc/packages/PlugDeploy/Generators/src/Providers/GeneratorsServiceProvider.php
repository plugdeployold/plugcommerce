<?php

namespace PlugDeploy\Generators\Providers;

use Illuminate\Support\ServiceProvider;

class GeneratorsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerPackageComponentAdminGenerator();
    }

    /**
     * Register the make:pivot generator.
     */
    private function registerPackageComponentAdminGenerator()
    {
        $this->app->singleton('command.laracasts.migrate.pivot', function ($app) {
            return $app['PlugDeploy\Generators\Commands\AdminMakeCommand'];
        });

        $this->commands('command.laracasts.migrate.pivot');
    }
}
