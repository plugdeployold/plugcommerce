<?php

namespace PlugDeploy\Generators\Commands;

use Illuminate\Console\Command;
use Illuminate\Container\Container;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;
use Laracasts\Generators\Migrations\NameParser;
use Laracasts\Generators\Migrations\SchemaParser;
use Laracasts\Generators\Migrations\SyntaxBuilder;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


/*
 *
 * TODO Adicionar nova route
 * TODO Adicionar datagrid na crudlisting
 * TODO History
 * TODO Apagar modulo
 *
 * */

/*

php artisan make:package:component:admin \
--migration="true" \
--factory="true" \
--seeder="true" \
--model="true" \
--proxy="true" \
--contract="true" \
--controller="true" \
--repository_interface="true" \
--repository="true" \
--view="true" \
--route="true"

 */

class AdminMakeCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:package:component:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new migration class and apply schema at the same time';

    /**
     * The filesystem instance.
     *
     * @var Filesystem
     */
    protected $files;

    /**
     * Meta information for the requested migration.
     *
     * @var array
     */
    protected $meta;

    /**
     * @var Composer
     */
    private $composer;

    protected $packageName = "";
    protected $componentName = "";
    protected $singularName = "";
    protected $pluralName = "";
    protected $tableName = "";
    protected $routePrefix = "";

    /**
     * Create a new command instance.
     *
     * @param Filesystem $files
     * @param Composer $composer
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct();

        // Variables
        $this->packageName = "WineCellar/StockSystem";
        $this->componentName = "Capacity";
        $this->singularName = "capacity";
        $this->pluralName = "capacities";
        $this->tableName = $this->pluralName; //$this->option('tableName');
        $this->routePrefix = $this->singularName;

        // return ucwords(Str::singular(Str::camel($this->meta['table'])));

        // File System
        $this->files = $files;

        // Composer
        $this->composer = app()['composer'];
    }


    /**
     * Alias for the fire method.
     *
     * In Laravel 5.5 the fire() method has been renamed to handle().
     * This alias provides support for both Laravel 5.4 and 5.5.
     */
    public function handle()
    {
        $this->fire();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        // Create Generator History
        $this->createGeneratorHistory($this->packageName, $this->componentName, $this->tableName, $this->routePrefix);

        if ($this->option('migration')) {
            $this->info("Start Migration");
            $this->makeMigration($this->packageName, $this->componentName, $this->tableName);
        }

        if ($this->option('factory')) {
            $this->info("Start Factory");
            $this->makeFactory($this->packageName, $this->componentName);
        }

        if ($this->option('seeder')) {
            $this->info("Start Seeder");
            $this->makeSeeder($this->packageName, $this->componentName);
        }

        if ($this->option('model')) {
            $this->info("Start Model");
            $this->makeModel($this->packageName, $this->componentName, $this->tableName);
        }

        if ($this->option('proxy')) {
            $this->info("Start Proxy");
            $this->makeProxy($this->packageName, $this->componentName);
        }

        if ($this->option('contract')) {
            $this->info("Start Contract");
            $this->makeContract($this->packageName, $this->componentName);
        }

        if ($this->option('controller')) {
            $this->info("Start Controller");
            $this->makeDirectory(base_path() . '/packages/WineCellar/StockSystem/src/Http/Controllers/Admin/' . $this->componentName . '/ToCREATE');
            $this->makeController($this->packageName, $this->componentName, $this->routePrefix);
        }

        if ($this->option('repository')) {
            $this->makeDirectory(base_path() . '/packages/WineCellar/StockSystem/src/Repositories/' . $this->componentName . '/ToCREATE');
            $this->info("Start Repository");
            $this->makeRepository($this->packageName, $this->componentName);
        }

        if ($this->option('repository_interface')) {
            $this->info("Start Repository Interface");
            $this->makeRepositoryInterface($this->packageName, $this->componentName);
        }

        if ($this->option('view')) {
            $this->info("Start View");
            $this->makeDirectory(base_path() . '/packages/WineCellar/StockSystem/src/Resources/views/admin/' . $this->singularName . '/ToCREATE');
            $this->makeView($this->packageName, $this->componentName, $this->routePrefix);
        }

        if ($this->option('route')) {
            $this->info("Start Route");
            $this->makeRoute($this->packageName, $this->componentName, $this->routePrefix);
        }

        //$this->composer->dumpAutoloads();
    }

    /**
     * Replace the class name in the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replacePackageName(&$stub, $packageName)
    {
        $stub = str_replace('{{ComponentName}}', $packageName, $stub);

        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replaceComponentName(&$stub, $componentName)
    {
        $stub = str_replace('{{ComponentName}}', $componentName, $stub);

        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replaceComponentNamePlural(&$stub, $componentName)
    {
        $stub = str_replace('{{ComponentNamePlural}}', Str::plural($componentName), $stub);

        return $this;
    }


    /**
     * Replace the class name in the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replaceTableName(&$stub, $tableName)
    {
        $stub = str_replace('{{TableName}}', $tableName, $stub);

        return $this;
    }

    /**
     * Replace the class name in the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replaceRoutePrefix(&$stub, $routePrefix)
    {
        $stub = str_replace('{{RoutePrefix}}', $routePrefix, $stub);

        return $this;
    }

    protected function createGeneratorHistory($packageName, $componentName, $tableName, $routePrefix)
    {
        $this->line("<info>Generator History</info>");
    }

    /**
     * Generate the desired migration.
     */
    protected function makeMigration($packageName, $componentName, $tableName)
    {
        // Create a path for the migration file
        $path = base_path() . '/packages/' . $packageName . '/src/Database/Migrations/' . date('Y_m_d_His') . '_create_' . $tableName . '_table.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/migration.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName)
            ->replaceComponentNamePlural($stub, $componentName)
            ->replaceTableName($stub, $tableName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Migration:</info> {$filename}");
    }

    protected function makeFactory($packageName, $componentName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Database/Factories/' . $componentName . 'Factory.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/factory.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Migration:</info> {$filename}");
    }

    protected function makeSeeder($packageName, $componentName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Database/Seeders/' . $componentName . 'Seeder.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/seeder.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Seeder:</info> {$filename}");
    }

    protected function makeModel($packageName, $componentName, $tableName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Models/' . $componentName . '.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/model.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName)
            ->replaceTableName($stub, $tableName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Model:</info> {$filename}");
    }

    protected function makeProxy($packageName, $componentName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Models/' . $componentName . 'Proxy.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/proxy.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Model:</info> {$filename}");
    }

    protected function makeContract($packageName, $componentName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Contracts/' . $componentName . '.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/contract.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Model:</info> {$filename}");
    }

    protected function makeController($packageName, $componentName, $routePrefix)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Http/Controllers/Admin/' . $componentName . '/' . $componentName . 'Controller.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/controller.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName)
            ->replaceRoutePrefix($stub, $routePrefix);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Controller:</info> {$filename}");
    }

    protected function makeRepository($packageName, $componentName)
    {
        $pathFolder = base_path() . '/packages/' . $packageName . '/src/Repositories/' . $componentName . '/';

        //$this->makeDirectory($pathFolder);

        $path = $pathFolder . '/' . $componentName . 'Repository.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/repository.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Repository:</info> {$filename}");
    }

    protected function makeRepositoryInterface($packageName, $componentName)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Repositories/' . $componentName . '/' . $componentName . 'RepositoryInterface.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/repositoryInterface.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Repository Interface:</info> {$filename}");
    }

    protected function makeView($packageName, $componentName, $routePrefix)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Resources/views/admin/' . $routePrefix . '/' . 'index.blade.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/view.stub');

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created View:</info> {$filename}");
    }

    protected function makeRoute($packageName, $componentName, $routePrefix)
    {
        $path = base_path() . '/packages/' . $packageName . '/src/Http/AutomatedRoutes/' . $routePrefix . '.php';

        // Open Migration Stub
        $stub = $this->files->get(__DIR__ . '/../stubs/route.stub');

        // Replace
        $this->replaceComponentName($stub, $componentName)
            ->replaceRoutePrefix($stub, $routePrefix);

        // Open Stub and create a file
        $this->files->put($path, $stub);

        // Create filename
        $filename = pathinfo($path, PATHINFO_FILENAME);

        // Show info
        $this->line("<info>Created Model:</info> {$filename}");
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param string $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (!$this->files->isDirectory(dirname($path))) {
            $this->files->makeDirectory(dirname($path), 0777, true, true);
        }

        $this->line(dirname($path));
        $this->line("<info>Error create directory</info>");
    }

    /**
     * Replace the schema for the stub.
     *
     * @param string $stub
     * @return $this
     */
    protected function replaceSchema(&$stub)
    {
        if ($schema = $this->option('schema')) {
            $schema = (new SchemaParser)->parse($schema);
        }

        $schema = (new SyntaxBuilder)->create($schema, $this->meta);

        $stub = str_replace(['{{schema_up}}', '{{schema_down}}'], $schema, $stub);

        return $this;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['packageName', null, InputOption::VALUE_OPTIONAL, '...', null],
            ['componentName', null, InputOption::VALUE_OPTIONAL, '...', null],
            ['migration', null, InputOption::VALUE_OPTIONAL, '...', null],
            ['tableName', null, InputOption::VALUE_OPTIONAL, '...', null],
            ['factory', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['seeder', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['model', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['proxy', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['contract', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['controller', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['repository', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['repository_interface', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['view', null, InputOption::VALUE_OPTIONAL, '...', false],
            ['route', null, InputOption::VALUE_OPTIONAL, '...', false],
        ];
    }
}
